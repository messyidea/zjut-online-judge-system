import os


basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you can not guess'
    #SSL_DISABLE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_CRECORD_QUERIES = True
    # ???
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    OJ_ADMIN = os.environ.get('FLASKY_ADMIN')
    OJ_PROBLEMS_PER_PAGE = 5
    BOOTSTRAP_SERVE_LOCAL = True
    SQLALCHEMY_ECHO = False
    UPLOAD_PATH = os.path.join(os.path.abspath('.'), 'Upload')
    STATIC_PATH = os.path.join(os.path.abspath('.'), 'app', 'static')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_') or \
    #    'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')
    SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost:3306/oj?charset=utf8'



config = {
    'development': DevelopmentConfig,

    'default': DevelopmentConfig
}