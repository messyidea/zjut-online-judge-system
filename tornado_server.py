#coding=utf-8
#!/usr/bin/python

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from manage import app
import sys, getopt

port = 5000
opts, args = getopt.getopt(sys.argv[1:], "p:")
for op, value in opts:
    if op == '-p':
        port = value

http_server = HTTPServer(WSGIContainer(app))
http_server.listen(port)
IOLoop.instance().start()
