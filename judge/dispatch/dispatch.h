//
// Created by messyidea on 16-3-27.
//

#ifndef DISPATCH_DISPATCH_H
#define DISPATCH_DISPATCH_H


struct conf_oj {
    char *oj_host;
    int oj_port;
    char *username;
    char *password;
    char *oj_home;
    char *oj_db;
    int oj_mod;
    int oj_remain;
};

struct conf_oj oj_config;

#define LOCKFILE "/var/tmp/dispatch.pid"
#define LOCKMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)


#endif //DISPATCH_DISPATCH_H
