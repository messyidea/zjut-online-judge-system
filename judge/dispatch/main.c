#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <mysql/mysql.h>
#include <string.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/stat.h>
#include <errno.h>

#include "dispatch.h"
#include "cJSON.h"
#include "logger.h"

#define MAX_CONF_SIZE 100000
#define MAX_JOB_COUNT 4
#define MAX_QUENE_SIZE 100
#define MAX_BUF_SIZE 1000
int DEBUG = 0;
char *config_path;

#define OJ_WAIT 0
#define OJ_RUN 1
#define OJ_AC 2
#define OJ_PE 3
#define OJ_WA 4
#define OJ_RE 5
#define OJ_TLE 6
#define OJ_MLE 7
#define OJ_OLE 8
#define OJ_CE 9
#define OJ_SE 10

MYSQL *conn;
MYSQL_RES *res;
MYSQL_ROW row;

int jobs[MAX_QUENE_SIZE];
pid_t running_jobs[MAX_JOB_COUNT];
int jobs_id[MAX_JOB_COUNT];
int running_count;

const char *Lang_src[] = {"Main.c", "Main.cpp", "Main.java", "Main.pass"};


// return 0 is ok
int executesql(MYSQL *conn, const char *sql) {
    if(mysql_real_query(conn, sql, strlen(sql))) {
        sleep(20);
        conn = NULL;
        return 1;
    } else {
        return 0;
    }
}


//return 0 is ok
int init_mysql_conn(MYSQL **conn) {
    if(*conn == NULL) {
        *conn = mysql_init(NULL);

        const char timeout = 30;
        mysql_options(*conn, MYSQL_OPT_CONNECT_TIMEOUT, &timeout);

        if(!mysql_real_connect(*conn, oj_config.oj_host, oj_config.username, oj_config.password, oj_config.oj_db, oj_config.oj_port, 0, 0)) {
            sleep(2);
            return 1;
        } else {
            LOG_DEBUG("Mysql connect ok");
            return 0;
        }
    } else {
        return executesql(*conn, "set names utf8");
    }
}


void read_config() {
    // get config
    char *buf;
    FILE *f = fopen(config_path, "r");
    if (f == NULL) {
        LOG_FATAL("Invalid config path.");
        exit(1);
    }

    fseek(f, 0, SEEK_END);
    long pos = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (pos >= MAX_CONF_SIZE) {
        LOG_FATAL("Too large config size.");
        exit(1);
    }

    buf = malloc(pos + 1);
    if (buf == NULL) {
        LOG_FATAL("No enough memory.");
        exit(1);
    }

    fread(buf, pos, 1, f);
    fclose(f);

    buf[pos] = '\0';

    oj_config.oj_mod = 1;
    oj_config.oj_remain = 0;

    // config parse
    cJSON *root = cJSON_Parse(buf);
    cJSON *son;
    son = cJSON_GetObjectItem(root,"oj_hostname");
    oj_config.oj_host = son->valuestring;
    son = cJSON_GetObjectItem(root,"oj_port");
    oj_config.oj_port = son->valueint;
    son = cJSON_GetObjectItem(root,"username");
    oj_config.username = son->valuestring;
    son = cJSON_GetObjectItem(root,"password");
    oj_config.password = son->valuestring;
    son = cJSON_GetObjectItem(root,"oj_home");
    oj_config.oj_home = son->valuestring;
    son = cJSON_GetObjectItem(root,"oj_db");
    oj_config.oj_db = son->valuestring;
    son = cJSON_GetObjectItem(root,"oj_mod");
    oj_config.oj_mod = son->valueint;
    son = cJSON_GetObjectItem(root,"oj_remain");
    oj_config.oj_remain = son->valueint;



    LOG_DEBUG("\n oj_host = %s\n oj_port = %d\n username = %s\n password = %s\n oj_home = %s\n oj_mod = %d\n oj_remain = %d\n", \
    oj_config.oj_host, oj_config.oj_port, oj_config.username, oj_config.password, oj_config.oj_home, oj_config.oj_mod, oj_config.oj_remain);

}

void init_arguments(int argc, char* argv[]) {
    int c;
    config_path = NULL;
    while((c = getopt(argc, argv, "hdc:")) != -1) {
        switch(c) {
            case 'h': {
                printf("-c config.json  // config\n");
                printf("-d              // deamon\n");
                printf("-h              // print this\n");
                exit(0);
            }
            case 'c': {
                config_path = optarg;
                break;
            }
            case 'd': {
                DEBUG = 1;
                break;
            }
            default: {
                LOG_DEBUG("Unknow option of %c\n", optopt);
                break;
            }
        }
    }
    if (config_path == NULL) {
        LOG_FATAL("Unknow config_path");
        exit(1);
    }

    // init other arguments
    running_count = 0;
}

// return the count of jobs
int get_job() {
    char *query = malloc(sizeof(char) * MAX_BUF_SIZE);
//    char *query = "SELECT id FROM submission WHERE result=0 AND MOD(id, %d) = %d ORDER BY id ASC limit 100";
    sprintf(query, "SELECT id FROM submission WHERE result=0 AND MOD(id, %d) = %d ORDER BY id ASC limit 100", oj_config.oj_mod, oj_config.oj_remain);

    if (mysql_real_query(conn,query,strlen(query))){
        sleep(20);
        return 0;
    }

    res=mysql_store_result(conn);
    int i=0, count = 0;

    while((row=mysql_fetch_row(res))!=NULL){
        jobs[i++]=atoi(row[0]);
    }
    count = i;
    while(i<MAX_QUENE_SIZE) jobs[i++]=0;

    return count;
}



int judge(int jobid, int runid) {

    runid ++;
    //local variable
    MYSQL *conn2 = NULL;
    MYSQL_RES *res2;
    MYSQL_ROW row2;

//    char *ppp = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
//    sprintf(ppp, "haha%d.txt", runid);
//    freopen(ppp, "w", stdout);

    //获取提交的代码
    char *temp = (char *)malloc(sizeof(char) * MAX_BUF_SIZE);
    char *query = (char *)malloc(sizeof(char) * MAX_BUF_SIZE);
    sprintf(query, "select submission.code,submission.language, problem.id, problem.time_limit,problem.memory_limit,problem.spj from submission left join problem on submission.problem_id = problem.id where submission.id = %d", jobid);

    init_mysql_conn(&conn2);

    if (mysql_real_query(conn2,query,strlen(query))){
        return 0;
    }
    res2=mysql_store_result(conn2);
    row2=mysql_fetch_row(res2);


    char *code_path = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    strcpy(code_path, oj_config.oj_home);
    //  /home/messyidea/judge
    sprintf(temp, "/run%d/", runid);
    strcat(code_path, temp);
    //  /home/messyidea/judge/run1/
    strcat(code_path, Lang_src[atoi(row2[1])]);
    //  /home/messyidea/judge/run1/Main.cpp


    LOG_DEBUG("job %d code path == %s\n", jobid, code_path);

    //把提交的代码写入文件
    FILE *fp = fopen(code_path, "w");
    if (fp == NULL) {
        LOG_FATAL("Invalid code path.");
        exit(1);
    }
    fwrite(row2[0], strlen(row2[0]) * sizeof(char), 1, fp);
    fclose(fp);

    //准备judge的参数
    char *data_dir = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    strcpy(data_dir, oj_config.oj_home);
    strcat(data_dir, "/problems/");
    strcat(data_dir, row2[2]);

    char *run_dir = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    strcpy(run_dir, oj_config.oj_home);
    sprintf(temp, "/run%d/", runid);
    strcat(run_dir, temp);

    LOG_DEBUG("\ndata_dir = %s\n run_dir = %s\n lang = %s\n timelimit = %s\n memorylimit = %s\n", data_dir, run_dir, row2[1], row2[3], row2[4]);

    mysql_free_result(res2);
    mysql_close(conn2);

    if(atoi(row2[5]) == 0)
        execl("./judge", "./judge", "-D", data_dir, "-R", run_dir, "-l", row2[1], "-t", row2[3], "-m", row2[4], (char*)NULL);
    else
        execl("./judge", "./judge", "-D", data_dir, "-R", run_dir, "-l", row2[1], "-t", row2[3], "-m", row2[4], "-S", (char*)NULL);
}

void update_result(int jobid, int runid) {

    runid ++;
    char *rst_path = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    strcpy(rst_path, oj_config.oj_home);
    //  /home/messyidea/judge
    char *temp = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    sprintf(temp, "/run%d/result", runid);
    strcat(rst_path, temp);
    //  /home/messyidea/judge/run1/result

    char *buf;
    FILE *f = fopen(rst_path, "r");
    if (f == NULL) {
        LOG_FATAL("Invalid result path %S.", rst_path);
        exit(1);
    }

    fseek(f, 0, SEEK_END);
    long pos = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (pos >= MAX_CONF_SIZE) {
        LOG_FATAL("Too large result size.");
        exit(1);
    }

    buf = malloc(pos + 1);
    if (buf == NULL) {
        LOG_FATAL("No enough memory.");
        exit(1);
    }

    fread(buf, pos, 1, f);
    fclose(f);

    buf[pos] = '\0';
    int rst, time, memory;
    sscanf(buf, "%d %d %d", &rst, &time, &memory);

    char *sql = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    sprintf(sql, "update submission set result=%d, time=%d, memory=%d where id = %d", rst, time, memory, jobid);

    executesql(conn, sql);

    // compile error
    if(rst == OJ_CE) {
        //read error date
        strcpy(rst_path, oj_config.oj_home);
        //  /home/messyidea/judge
        sprintf(temp, "/run%d/compile_error.txt", runid);
        strcat(rst_path, temp);
        //  /home/messyidea/judge/run1/compile_error.txt

        char *cbuf;
        FILE *f = fopen(rst_path, "r");
        if (f == NULL) {
            LOG_FATAL("Invalid cerror path %S.", rst_path);
            exit(1);
        }

        fseek(f, 0, SEEK_END);
        pos = ftell(f);
        fseek(f, 0, SEEK_SET);

        if (pos >= MAX_CONF_SIZE) {
            LOG_FATAL("Too large result size.");
            exit(1);
        }

        cbuf = malloc(pos + 1);
        if (cbuf == NULL) {
            LOG_FATAL("No enough memory.");
            exit(1);
        }

        fread(cbuf, pos, 1, f);
        fclose(f);

        cbuf[pos] = '\0';
        sql = (char*)malloc(sizeof(char) * 65536);
        char *sqlend = sql;
        strcpy(sqlend, "update submission set info='");
        sqlend += strlen(sql);
        sqlend += mysql_real_escape_string(conn, sqlend, cbuf, strlen(cbuf));
        sqlend += sprintf(sqlend, "' where id = '%d'", jobid);
        *sqlend = 0;
        LOG_DEBUG("sql == %s", sql);
        executesql(conn, sql);
    }


    if (mysql_real_query(conn,sql,strlen(sql))){
        LOG_FATAL("MYSQL ERROR");
    }else{
        if(mysql_affected_rows(conn)>0ul)
            LOG_DEBUG("affected");
        else
            LOG_DEBUG("no affected");
    }

}

/* set advisory lock on file */
int lockfile(int fd)
{
    struct flock fl;

    fl.l_type = F_WRLCK;  /* write lock */
    fl.l_start = 0;
    fl.l_whence = SEEK_SET;
    fl.l_len = 0;  //lock the whole file

    return(fcntl(fd, F_SETLK, &fl));
}

int already_running(const char *filename)
{
    int fd;
    char buf[16];

    fd = open(filename, O_RDWR | O_CREAT, LOCKMODE);
    if (fd < 0) {
        printf("can't open %s: %m\n", filename);
        exit(1);
    }

    /* 先获取文件锁 */
    if (lockfile(fd) == -1) {
        if (errno == EACCES || errno == EAGAIN) {
            printf("file: %s already locked", filename);
            close(fd);
            return 1;
        }
        printf("can't lock %s: %m\n", filename);
        exit(1);
    }
    /* 写入运行实例的pid */
    ftruncate(fd, 0);
    sprintf(buf, "%ld", (long)getpid());
    write(fd, buf, strlen(buf) + 1);
    return 0;
}

void demonize() {

    pid_t pid, sid;

    pid = fork();
    if(pid < 0) {
        exit(-1);
    }

    if(pid > 0) {
        // parents
        exit(0);
    }

    umask(0);

    sid = setsid();
    if(sid < 0) {
        exit(-1);
    }


    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

}


int run() {
    int count = get_job();

    LOG_DEBUG("%d jobs gets!\n", count);

    if(count == 0) return 0;

    pid_t t_pid = 0;
    int status;

    char *sql = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
//    sprintf(sql, "update submission set result=%d, time=%d, memory=%d where id = %d", rst, time, memory, jobid);
//    executesql(conn, sql);

    int i, pos;
    for(i = 0; jobs[i] != 0; ++i) {
        LOG_DEBUG("get submission %d from quene \n", jobs[i]);

        if(running_count >= MAX_JOB_COUNT) {
            t_pid = waitpid(-1, NULL, 0);
            for(pos = 0; pos < MAX_JOB_COUNT; ++pos) {
                if(running_jobs[pos] == t_pid) break;
            }

            update_result(jobs_id[pos], pos);
            --running_count;
            running_jobs[pos] = 0;
            LOG_DEBUG("end job %d at room %d", jobs_id[pos], pos);
        } else {
            for(pos = 0; pos < MAX_JOB_COUNT; ++pos) {
                if(running_jobs[pos] == 0) {
                    break;
                }
            }
        }

        // running_jobs[pos] = 0 now
        //clear sql
        strcpy(sql, "");
        sprintf(sql, "update submission set result=%d where id=%d", OJ_RUN, jobs[i]);
//        if(!executesql(conn, sql)) continue;
        executesql(conn, sql);

        running_jobs[pos] = fork();
        if(running_jobs[pos] == 0) {
            //son
            judge(jobs[i], pos);
            exit(0);
        }
        jobs_id[pos] = jobs[i];
        running_count ++;
        LOG_DEBUG("start a new job %d at room %d\n", jobs[i], pos);
    }

    while((t_pid = waitpid(-1, &status, 0)) > 0) {
        for(pos = 0; pos < MAX_JOB_COUNT; ++pos) {
            if(running_jobs[pos] == t_pid) break;
        }

        update_result(jobs_id[pos], pos);
        --running_count;
        running_jobs[pos] = 0;
        LOG_DEBUG("final end job %d at room %d\n", running_jobs[pos], pos);
    }

    return 1;

}


int main(int argc, char* argv[]) {

    log_open("dispatch.log");

    init_arguments(argc, argv);

    if(DEBUG != 0) {
        demonize();
    }

    if(already_running(LOCKFILE)) {
        LOG_FATAL("Already running");
        exit(-1);
    }

    read_config();


    int flag = 1;
    while(1) {
        //run;
        while(flag && !init_mysql_conn(&conn)) {
            //begian run
            flag = run();
        }

        sleep(5);
        flag = 1;
    }

    return 0;
}