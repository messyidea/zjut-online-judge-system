#ifndef JUDGE_H
#define JUDGE_H

#define MAX_CONF_SIZE 16 * 1024

struct conf_oj {
    char *oj_host;
    int oj_port;
    char *username;
    char *password;
};

struct param {
//    char *oj_home;
    char *data_dir;
    char *run_dir;
    int lang;
    int time_limit;
    int memory_limit;
    int output_limit;
    int SPJ;
};


struct info_submission {
    int submission_id;
    int user_id;
    int problem_id;
    int result;
    int spj;
};

struct LangSupport {
    const char* const Compilecmd[20];
    const char* const RunCmd[20];
};

const struct LangSupport CLang = {
        {"gcc", "Main.c", "-o", "Main", "-Wall", "-lm",
        "-std=c99", "-DONLINE_JUDGE", NULL},
        {"./Main", NULL}
};

const struct LangSupport CppLang = {
        {"g++","Main.cpp","-o", "Main", "-std=c++98", "-O2",NULL},
        {"./Main", NULL}
};

const struct LangSupport JavaLang = {
        { "javac", "-J-Xms128M", "-J-Xmx512M", "Main.java", NULL },
//        { "/usr/java/jdk1.8.0_73/bin/java", "replace", "-Djava.security.manager", "-Xms128M", "-Xms512M", "-DONLINE_JUDGE=true", "Main", NULL }
        { "java", "replace", "-Djava.security.manager", "-Xms128M", "-Xms512M", "-DONLINE_JUDGE=true", "Main", NULL }
};

const struct LangSupport *Langs[] = {
        &CLang,
        &CppLang,
        &JavaLang
};


struct conf_oj oj_config;
struct param options;

int malarm(int which, int milliseconds);
void out_result(int rst, int time, int memory);


//void demonize(const char* path) {
//
//    /* Our process ID and Session ID */
//    pid_t pid, sid;
//
//    /* Fork off the parent process */
//    pid = fork();
//    if (pid < 0) {
//        exit(EXIT_FAILURE);
//    }
//
//    /* If we got a good PID, then
//       we can exit the parent process. */
//    if (pid > 0) {
//        FILE *file = fopen(path, "w");
//        if (file == NULL) FATAL("Invalid pid file\n");
//
//        fprintf(file, "%d", pid);
//        fclose(file);
//        exit(EXIT_SUCCESS);
//    }
//
//    /* Change the file mode mask */
//    umask(0);
//
//    /* Open any logs here */
//
//    /* Create a new SID for the child process */
//    sid = setsid();
//    if (sid < 0) {
//        /* Log the failure */
//        exit(EXIT_FAILURE);
//    }
//
//    /* Change the current working directory */
//    if ((chdir("/")) < 0) {
//        /* Log the failure */
//        exit(EXIT_FAILURE);
//    }
//
//    /* Close out the standard file descriptors */
//    close(STDIN_FILENO);
//    close(STDOUT_FILENO);
//    close(STDERR_FILENO);
//}

#endif