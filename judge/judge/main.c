#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <getopt.h>
#include <ctype.h>

#include "cJSON.h"
#include "judge.h"
#include "runner.h"
#include "logger.h"

#define OJ_WAIT 0
#define OJ_RUN 1
#define OJ_AC 2
#define OJ_PE 3
#define OJ_WA 4
#define OJ_RE 5
#define OJ_TLE 6
#define OJ_MLE 7
#define OJ_OLE 8
#define OJ_CE 9
#define OJ_SE 10

#define MAX_BUF_SIZE 1000

char *short_opts = "hD:R:l:t:m:o:S";

static struct option long_opts[] = {
        {"help", no_argument, NULL, 'h'},
        {"DataDir", required_argument, NULL, 'D'},
        {"RunDir", required_argument, NULL, 'R'},
        {"lang", required_argument, NULL, 'l'},
        {"time_limit", required_argument, NULL, 't'},
        {"memory_limit", required_argument, NULL, 'm'},
        {"output_limit", required_argument, NULL, 'o'},
        {"Spj", no_argument, NULL, 'S'},
        {0, 0, 0, 0}
};


//void init_param() {
//    char *buf;
//    FILE *f = fopen("/home/messyidea/config.json", "r");
//    if (f == NULL) FATAL("Invalid config path.");
//
//    fseek(f, 0, SEEK_END);
//    long pos = ftell(f);
//    fseek(f, 0, SEEK_SET);
//
//    if (pos >= MAX_CONF_SIZE) FATAL("Too large config size.");
//
//    buf = malloc(pos + 1);
//    if (buf == NULL) FATAL("No enough memory.");
//
//    fread(buf, pos, 1, f);
//    fclose(f);
//
//    buf[pos] = '\0';
//
//    // transfer to config
//    cJSON *root = cJSON_Parse(buf);
//    cJSON *son;
//    son = cJSON_GetObjectItem(root,"oj_hostname");
//    oj_config.oj_host = son->valuestring;
//    son = cJSON_GetObjectItem(root,"oj_port");
//    oj_config.oj_port = son->valueint;
//    son = cJSON_GetObjectItem(root,"username");
//    oj_config.username = son->valuestring;
//    son = cJSON_GetObjectItem(root,"password");
//    oj_config.password = son->valuestring;
//
//    if (verbose) {
//        printf("oj_host = %s\n", oj_config.oj_host);
//        printf("oj_port = %d\n", oj_config.oj_port);
//        printf("username = %s\n", oj_config.username);
//        printf("password = %s\n", oj_config.password);
//    }
//
//}

int Compile() {
    int status = 0;
    pid_t compiler = fork();

    if (compiler < 0) {
        LOG_FATAL("Compiler error on fork");
        exit(1);
    }

    if (compiler == 0) {
        // son
        chdir(options.run_dir);

        freopen("compile_error.txt", "w", stderr);
        freopen("/dev/null", "w", stdout);

        malarm(ITIMER_REAL, 60000);

        execvp(Langs[options.lang]->Compilecmd[0], (char * const *)Langs[options.lang]->Compilecmd);
        exit(99);
    } else {
        // parents
        waitpid(compiler, &status, 0);
        LOG_DEBUG("Compile return status %d", status);
        return status;
    }

}

void timeout() {
    ;
}

int malarm(int which, int milliseconds) {
    struct itimerval it;
    it.it_value.tv_sec = milliseconds / 1000;
    it.it_value.tv_usec = (milliseconds % 1000) * 1000;
    it.it_interval.tv_sec = 0;
    it.it_interval.tv_usec = 0;
    return setitimer(which, &it, NULL);
}


void parse_param(int argc,char* argv[]) {
    //初始化参数
    char c;
    while((c = (char)getopt_long (argc, argv, short_opts, long_opts, NULL)) != -1) {
        switch(c) {
            case 'h': {
//                print_usage();
                exit(0);
            }
            case 'D': {
                options.data_dir = optarg;
                break;
            }
            case 'R': {
                options.run_dir = optarg;
                break;
            }
            case 'S': {
                options.SPJ = 1;
                break;
            }
            case 'l': {
                sscanf(optarg, "%d", &options.lang);
                break;
            }
            case 't': {
                sscanf(optarg, "%d", &options.time_limit);
                break;
            }
            case 'm': {
                sscanf(optarg, "%d", &options.memory_limit);
                break;
            }
            case 'o': {
                sscanf(optarg, "%d", &options.output_limit);
                break;
            }
            default: {
                LOG_DEBUG("unknow option of %c", optopt);
                break;
            }
        }
    }

    if(options.SPJ != 1) options.SPJ = 0;
    int flag = 0;
    if(options.data_dir == NULL || strcmp(options.data_dir, "") == 0) flag = 1;
    if(options.run_dir == NULL || strcmp(options.run_dir, "") == 0) flag = 1;
    if(! (options.lang >= 0 && options.lang <= 2)) flag = 1;
//    printf("%s %s %d %d\n", options.data_dir, options.run_dir, options.lang, flag);
    if(flag) {
        out_result(OJ_SE, 0, 0);
        LOG_FATAL("Wrong argument");
        exit(1);
    }


}

void find_next_nonspace(int *c1, int *c2, FILE **f1, FILE **f2, int *ret) {
    while((isspace(*c1) || (isspace(*c2)))) {
        if(*c1 != *c2) {
            if(*c2 == EOF) {
                do {
                    *c1 = fgetc(*f1);
                } while(isspace(*c1));
                continue;
            } else if (*c1 == EOF) {
                do {
                    *c2 = fgetc(*f2);
                } while(isspace(*c2));
                continue;
            } else if(*c1 == '\r' && *c2 == '\n') {
                *c1 = fgetc(*f1);
            } else if(*c2 == '\r' && *c1 == '\n') {
                *c2 = fgetc(*f2);
            } else {
                *ret = OJ_PE;
            }
        }
        if(isspace(*c1)) {
            *c1 = fgetc(*f1);
        }
        if(isspace(*c2)) {
            *c2 = fgetc(*f2);
        }
    }
}

int do_compair(const char *file1, const char *file2) {
    int ret = OJ_AC;
    int c1, c2;
    FILE *f1 = fopen(file1, "r");
    FILE *f2 = fopen(file2, "r");
    if(!f1 || !f2) {
        ret = OJ_RE;
    } else {
        for(;;) {
            c1 = fgetc(f1);
            c2 = fgetc(f2);
            find_next_nonspace(&c1, &c2, &f1, &f2, &ret);
            for(;;) {
                while((!isspace(c1) && c1) || (!isspace(c2) && c2)) {
                    if(c1 == EOF && c2 == EOF) {
                        goto end;
                    }
                    if(c1 == EOF || c2 == EOF) {
                        break;
                    }
                    if(c1 != c2) {
                        ret = OJ_WA;
                        goto end;
                    }
                    c1 = fgetc(f1);
                    c2 = fgetc(f2);
                }
                find_next_nonspace(&c1, &c2, &f1, &f2, &ret);
                if(c1 == EOF && c2 == EOF) {
                    goto end;
                }
                if(c1 == EOF || c2 == EOF) {
                    ret = OJ_WA;
                    goto end;
                }

                if((c1 == '\n' || !c1) && (c2 == '\n' || !c2)) {
                    break;
                }
            }
        }
    }

    end:
    if(f1) fclose(f1);
    if(f2) fclose(f2);
    return ret;

}

void judge() {
    // 切换到运行目录
    chdir(options.run_dir);

    //初始化judge_config和judge_result结构体
    int judge_rst = 0;
    struct config *judge_config = malloc(sizeof(struct config));
    judge_config->args[0] = NULL;
    int pos = 0;
    while(Langs[options.lang]->RunCmd[pos] != NULL) {
        judge_config->args[pos] = (char*)Langs[options.lang]->RunCmd[pos];
        pos ++;
    }
    judge_config->args[pos] = NULL;
    judge_config->in_file = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    sprintf(judge_config->in_file, "%s/data/in", options.data_dir);

    LOG_DEBUG("in file path is %s", judge_config->in_file);

    judge_config->out_file = "out";
    judge_config->max_cpu_time = options.time_limit;
    judge_config->max_memory = options.memory_limit * 1024 * 1024;
    judge_config->use_nobody = 0;
    judge_config->use_sandbox = 1;
    judge_config->path = (char *)Langs[options.lang]->RunCmd[0];
    judge_config->env[0] = NULL;


    // for java
    if(options.lang == 2) {
        judge_config->use_sandbox = 0;
        char *java_memory_limit = malloc(sizeof(char) * MAX_BUF_SIZE);
        sprintf(java_memory_limit, "-Xmx%dM", options.memory_limit * 3);
        judge_config->max_memory = -1;
        judge_config->args[1] = java_memory_limit;
    }



    struct result *judge_result = malloc(sizeof(struct result));

    run(judge_config, judge_result);

    if(options.lang == 2 && judge_result->memory > options.memory_limit*1024*1024*1.5) {
        judge_result->flag = MEMORY_LIMIT_EXCEEDED;
    }

    if (options.SPJ != 1) {
//        printf("flag == %d %d\n", judge_result->flag, SUCCESS);
        if (judge_result->flag == SUCCESS) {
            char *path1, *path2;
            path1 = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
            path2 = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
            sprintf(path1, "%s/data/out", options.data_dir);
            sprintf(path2, "%s/out", options.run_dir);
            judge_rst = do_compair(path1, path2);
        } else if(judge_result->flag == CPU_TIME_LIMIT_EXCEEDED){
            judge_rst = OJ_TLE;
        } else if(judge_result->flag == MEMORY_LIMIT_EXCEEDED) {
            judge_rst = OJ_MLE;
        } else if(judge_result->flag == RUNTIME_ERROR) {
            judge_rst = OJ_RE;
        } else {
            judge_rst = OJ_SE;
        }
    } else {
        // SPJ
        LOG_DEBUG("Start SPJ");
        char *spjpath, *outpath;
        spjpath = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
        outpath = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
        sprintf(spjpath, "%s/spj", options.data_dir);
        sprintf(outpath, "%s/out", options.run_dir);
        LOG_DEBUG("spjpath = %s; outpath = %s", spjpath, outpath);
        int status = 0;
        pid_t spjpid = fork();
        if (spjpid < 0) {
            LOG_FATAL("SPJ ERROR ON FORK");
        }
        if(spjpid == 0) {
            //son
            malarm(ITIMER_REAL, 60000);
            execlp(spjpath, spjpath, outpath, 0);
            exit(99);
        } else {
            waitpid(spjpid, &status, 0);
            LOG_DEBUG("spj return status %d", status);
            if(status != 0) {
                judge_rst = OJ_WA;
            } else {
                judge_rst = OJ_AC;
            }
        }
    }

    LOG_DEBUG("Judge result is %d, begain write result", judge_rst);
    out_result(judge_rst, judge_result->cpu_time, (int)judge_result->memory);

}

// 写结果到文件
void out_result(int rst, int time, int memory) {
    chdir(options.run_dir);

    FILE *fp = fopen("result", "w");
    if(fp == NULL) {
        LOG_FATAL("Can't write result!");
        exit(1);
    }

    char *result = (char*)malloc(sizeof(char) * MAX_BUF_SIZE);
    sprintf(result, "%d %d %d", rst, time, memory);

    fwrite(result, strlen(result)*sizeof(char), 1, fp);
    fclose(fp);
    exit(0);
}

void init() {
    // 切换到运行目录
    chdir(options.run_dir);

    log_open("judger.log");
}

int main(int argc, char* argv[])
{
    parse_param(argc, argv);

    init();

    int rst = Compile();

    if(rst != 0) {
        LOG_DEBUG("Judge result is %d, begain write result", OJ_CE);
        out_result(OJ_CE, 0, 0);
    }

    judge();

    return 0;
}
