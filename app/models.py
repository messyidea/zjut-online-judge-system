from . import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin, AnonymousUserMixin
from flask import current_app, request, abort
from . import login_manager
from hashlib import md5
from itsdangerous import Serializer
from sqlalchemy import func

class Permission:
    SUBMIT_ANSWER = 0x01
    REPLY = 0x02
    PUBLISH_PROBLEM = 0x04
    PUBLISH_NEWS = 0x08
    ADMINISTER = 0x80


class Privilege(db.Model):
    __tablename__ = 'privilege'
    user_id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.Integer, primary_key=True)


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role', lazy='dynamic')
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)

    @staticmethod
    def insert_roles():
        roles = {
            'Administrator': (0xff, False, 1),
            'Moderator': (Permission.SUBMIT_ANSWER | Permission.REPLY | Permission.PUBLISH_PROBLEM | Permission.PUBLISH_NEWS, False, 2),
            'User': (Permission.SUBMIT_ANSWER | Permission.REPLY, True, 3),
            'Ban': (0, False, 4)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            role.id = roles[r][2]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role %r>' % self.name


class Difficulty:
    PRO_EASY = 0
    PRO_USUAL = 1
    PRO_DIFFICULT = 2
    PRO_CRAZY = 3
    CHOICES = [(0, 'Easy'), (1, 'Usual'), (2, 'Hard'), (3, 'Crazy')]

class Problem(db.Model):
    __tablename__ = 'problem'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    time_limit = db.Column(db.Integer)
    memory_limit = db.Column(db.Integer)
    total_accepted = db.Column(db.Integer, default=0)
    total_submited = db.Column(db.Integer, default=0)
    pro_desc = db.Column(db.Text)
    pro_input = db.Column(db.Text)
    pro_output = db.Column(db.Text)
    sample_input = db.Column(db.Text)
    sample_output = db.Column(db.Text)
    hint = db.Column(db.Text)
    source = db.Column(db.Text)
    create_time = db.Column(db.DateTime, default=datetime.now, index=True)
    spj = db.Column(db.Boolean, default=False)
    # create user
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    visible = db.Column(db.Boolean, default=True)
    difficulty = db.Column(db.Integer, default=0)
    submissions = db.relationship('Submission', backref='problem')
    contest_problems = db.relationship('Contest_problem', backref='problem')
    topics = db.relationship('Topic', backref='problem')


    def to_json(self):
        json_pro = {
            'title': self.title,
            'pro_desc': self.pro_desc
        }
        return json_pro

    @staticmethod
    def gen(count=20):
        from sqlalchemy.exc import IntegrityError
        from random import seed
        import forgery_py

        seed()
        for i in range(count):
            p = Problem()
            p.id = 1000 + i
            p.title = 'a+b'
            p.time_limit = 1000
            p.memory_limit = 512
            p.total_accepted = 0
            p.total_submited = 0
            p.pro_desc = 'please cal a + b'
            p.pro_input = 'a b'
            p.pro_output = 'c'
            p.sample_input = '1 2'
            p.sample_output = '3'
            p.hint = 'haha'
            p.source = 'hehe'
            p.visible = True
            p.create_time = forgery_py.date.date(True)
            db.session.add(p)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()



    def __repr__(self):
        return '<Problem %r>' % self.title

class Language:
    L_C = 0
    L_C__ = 1
    L_JAVA = 2
    L_PASKAL = 3
    CHOICES = [(0, 'C'), (1, 'C++'), (2, 'JAVA')]


class Submission(db.Model):
    __tablename__ = 'submission'
    id = db.Column(db.Integer, primary_key=True)
    # user
    create_time = db.Column(db.DateTime, default=datetime.now, index=True)
    result = db.Column(db.Integer, default=0)
    language = db.Column(db.Integer)
    code = db.Column(db.Text)
    contest_id = db.Column(db.Integer, default=0)
    info = db.Column(db.Text)
    judge_time = db.Column(db.DateTime, default=datetime.now, index=True)
    time = db.Column(db.Integer, default=0)
    memory = db.Column(db.Integer, default=0)
    code_length = db.Column(db.Integer, default=0)
    shared = db.Column(db.Boolean, default=True)
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def to_json(self):
        json_rst = {
            "id": self.id,
            "result": self.result,
            "language": self.language,
            "time": self.time,
            "memory": self.memory,
            "code_length": self.code_length,
            "problem_id": self.problem_id,
            "create_time": str(self.create_time)
        }
        return json_rst

    def __init__(self, **kwargs):
        super(Submission, self).__init__(**kwargs)


    def __repr__(self):
        return '<Submission %r>' % self.id



class News(db.Model):
    __tablename__ = 'news'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    content = db.Column(db.Text())
    create_time = db.Column(db.DateTime, default=datetime.now, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    is_delete = db.Column(db.Boolean, default=False)
    is_show = db.Column(db.Boolean, default=False)
    replys = db.relationship('Reply', backref='news')

    def __init__(self, **kwargs):
        super(News, self).__init__(**kwargs)





class Topic(db.Model):
    __tablename__ = 'topic'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    content = db.Column(db.Text())
    create_time = db.Column(db.DateTime, default=datetime.now, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    contest_id = db.Column(db.Integer, db.ForeignKey('contest.id'))
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
    contest_problem_id = db.Column(db.Integer, default=1)
    is_delete = db.Column(db.Boolean, default=False)
    replys = db.relationship('Reply', backref='topic')

    def __init__(self, **kwargs):
        super(Topic, self).__init__(**kwargs)



class Reply(db.Model):
    __tablename__ = 'reply'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text())
    create_time = db.Column(db.DateTime, default=datetime.now, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    is_delete = db.Column(db.Boolean, default=False)
    topic_id = db.Column(db.Integer, db.ForeignKey('topic.id'))
    news_id =  db.Column(db.Integer, db.ForeignKey('news.id'))



    def __init__(self, **kwargs):
        super(Reply, self).__init__(**kwargs)



CONTEST_NOT_START = 0
CONTEST_RUNNING = 1
CONTEST_END = 2

# problem_contest = db.Table('problem_contest',
#                            db.Column('contest_id', db.Integer, db.ForeignKey('contest.id')),
#                            db.Column('problem_id', db.Integer, db.ForeignKey('problem.id'))
#                            )


class Contest_problem(db.Model):
    __tablename__ = 'contest_problem'
    pid = db.Column(db.Integer, db.ForeignKey('problem.id'), primary_key=True)
    cid = db.Column(db.Integer, db.ForeignKey('contest.id'), primary_key=True)
    id = db.Column(db.Integer)
    title = db.Column(db.String(64))

    def to_json(self):
        rst_json = {
            'id': self.pid,
            'title': self.title
        }
        return rst_json



class Contest_type:
    PUBLIC = 0
    PRIVATE = 1
    CHOICES = [(0, 'Public'), (1, 'Private')]

class Contest(db.Model):
    __tablename__ = 'contest'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    type = db.Column(db.Boolean, default=False)
    password = db.Column(db.String(64))
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    visible = db.Column(db.Boolean)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    #problems = db.relationship('Problem', secondary=problem_contest, backref=db.backref('contests', lazy='dynamic'), lazy='dynamic')
    problems = db.relationship('Contest_problem', backref='contest')
    topics = db.relationship('Topic', backref='contest')

    def to_json(self):
        # print "contest.starttime == ", self.start_time
        if self.type == True:
            type = 1
        else:
            type = 0

        if self.visible == True:
            visible = 1
        else:
            visible = 0
        json_contest = {
            'id': self.id,
            'title': self.title,
            'type': type,
            'start_time': str(self.start_time),
            'end_time': str(self.end_time),
            'visible': visible
        }
        return json_contest

    @property
    def status(self):
        now = datetime.now()
        if self.start_time > now:
            return CONTEST_NOT_START
        elif self.end_time < now:
            return CONTEST_END
        else:
            return CONTEST_RUNNING



class Gender:
    G_M = 0
    G_F = 1
    CHOICES = [(0, 'boy'), (1, 'girl')]




class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, nullable=False , index=True)
    email = db.Column(db.String(200), nullable=False, unique=True, index=True)
    password_hash = db.Column(db.String(128), nullable=False)
    gender = db.Column(db.Boolean, default=False)
    default_language = db.Column(db.Integer, default=0)
    school = db.Column(db.String(128))
    college = db.Column(db.String(128))
    major = db.Column(db.String(128))
    grade = db.Column(db.String(128))
    uclass = db.Column(db.String(128))
    qq = db.Column(db.String(128))
    phone = db.Column(db.String(128))
    address = db.Column(db.String(128))
    avatar = db.Column(db.String(128))
    nickname = db.Column(db.String(128))
    about_me = db.Column(db.Text())
    blog = db.Column(db.String(128))
    total_submit = db.Column(db.Integer, default=0)
    total_accept = db.Column(db.Integer, default=0)

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    problem = db.relationship('Problem', backref='user')
    submission = db.relationship('Submission', backref='user')
    news = db.relationship('News', backref='user')
    topic = db.relationship('Topic', backref='user')
    contest = db.relationship('Contest', backref='user')
    reply = db.relationship('Reply', backref='user')
    #active = db.Column(db.DateTime, default=datetime.now, index=True)
    #token = db.Column(db.String(20))

    def to_json(self):
        json_user = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'nickname': self.nickname,
            'role_id': self.role_id
        }
        return json_user

    def avatar(self, size):
        return 'http://gravatar.duoshuo.com/avatar/' + md5(self.email).hexdigest() +'?d=mm&s=' + str(size)

    def generate_auth_token(self, expiration):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])


    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @property
    def rank(self):
        # return 123
        ranks = db.session.execute('SELECT count(*) FROM `users` WHERE total_accept>:c', {'c':self.total_accept}).fetchall()
        # print "rank == ", ranks[0][0]
        return  ranks[0][0] + 1

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['OJ_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()


    def can(self, permisssions):
        return self.role is not None and (self.role.permissions & permisssions) == permisssions

    def have_access_to(self, base, offset):
        # print "cid == ", cid
        nowkey = (base << 24) + int(offset)
        # print nowkey
        key = Privilege.query.filter_by(user_id=self.id).filter_by(key=nowkey).first()
        return key != None

    # def have_access_to2(self, cid):
    #     print "cid == ", cid
    #     nowkey = (1<<24) + int(cid)
    #     print nowkey
    #     key = Privilege.query.filter_by(user_id=self.id).filter_by(key=nowkey).first()
    #     if key == None:
    #         abort(403)

    @property
    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    @property
    def is_moderator(self):
        return self.can(Permission.SUBMIT_ANSWER | Permission.REPLY | Permission.PUBLISH_PROBLEM | Permission.PUBLISH_NEWS)

    @staticmethod
    def be_admin(name):
        user = User.query.filter_by(username=name).first()
        user.role = Role.query.filter_by(permissions=0xff).first()
        db.session.add(user)
        db.session.commit()

    def __repr__(self):
        return '<User %r>' % self.username


class AnonymousUser(AnonymousUserMixin):
    def can(self, permission):
        return False

    def is_administrator(self):
        return False

    @staticmethod
    def default_language():
        return 0

login_manager.anonymous_user = AnonymousUser

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


