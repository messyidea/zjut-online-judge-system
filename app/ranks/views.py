from flask import render_template, redirect, request, url_for, flash, current_app
from flask.ext.login import login_required, current_user
from . import ranks
from .. import db
from ..models import User, Problem, Submission, Language, News


@ranks.route('/', methods=['GET', 'POST'])
def list():
    page = request.args.get('page', 1, type=int)
    pagination = User.query.order_by(User.total_accept.desc()).paginate(
        page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
        error_out=False
    )
    ranks = pagination.items
    #print ranks
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('ranks/list.html', ranks=ranks, pagination=pagination, newslist=newslist, start=current_app.config['OJ_PROBLEMS_PER_PAGE']*(page-1))