from flask import Blueprint

ranks = Blueprint('ranks', __name__)

from . import views, forms
