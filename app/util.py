from functools import wraps
from flask import abort, jsonify
from flask.ext.login import current_user
from .models import Permission


def pagination_to_json(pagination):
    results = [per.to_json() for per in pagination.items]
    _data = {
        "results": results,
        "has_prev": pagination.has_prev,
        "has_next": pagination.has_next,
        "current_page": pagination.page,
        "total_page": pagination.pages,
        "total": pagination.total
    }
    data = {
        "code": 0,
        "data": _data
    }

    return jsonify(data)


def message_to_json(code, message="ok"):
    data = {
        "code": code,
        "data": message
    }
    return jsonify(data)


def item_to_json(item):
    data = {
        "code": 0,
        "data": item.to_json()
    }
    # print "data.data.starttime == ", data
    return jsonify(data)


def items_to_json(items):
    results = [per.to_json() for per in items]
    data = {
        "code": 0,
        "data": results
    }
    return jsonify(data)
