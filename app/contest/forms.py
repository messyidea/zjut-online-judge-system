from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField, IntegerField, TextAreaField, HiddenField, SelectField, DateTimeField, FieldList, FormField
from wtforms.validators import Required, Length, Email, DataRequired, Regexp, EqualTo
from wtforms import ValidationError
from ..models import Contest_type, Language
from datetime import datetime


class Singleproblem(Form):
    def __init__(self, csrf_enabled=False, *args, **kwargs):
        super(Singleproblem, self).__init__(csrf_enabled=csrf_enabled, *args, **kwargs)
    id = IntegerField('id')
    title = StringField('name')


class AddContestForm(Form):
    title = StringField('Title')
    type = SelectField('Type', coerce=int, choices=Contest_type.CHOICES, default=0)
    password = PasswordField('Password')
    start_time = DateTimeField('start time', format="%Y-%m-%dT%H:%M:%S", default=datetime.today())
    end_time = DateTimeField('end time', format="%Y-%m-%dT%H:%M:%S", default=datetime.today())
    visible = BooleanField('visible')
    problem_list = HiddenField('problem_list')
    # problem_list = FieldList(FormField(Singleproblem), label='list', min_entries=2)
    submit = SubmitField('Submit')


class SubmitForm(Form):
    language = SelectField('Language', coerce=int, choices=Language.CHOICES)
    code = HiddenField('Code')
    submit = SubmitField('Submit')

class AddTopicForm(Form):
    problem =IntegerField('Problem')
    title = StringField('Title')
    content = TextAreaField('Content')
    submit = SubmitField('Submit')


class ReplyForm(Form):
    content = TextAreaField('Content')
    submit = SubmitField('Submit')


class PasswordForm(Form):
    password = StringField('Password')
    submit = SubmitField('Submit')