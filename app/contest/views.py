from flask import render_template, redirect, request, url_for, flash, current_app, abort, session
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import contest
from .forms import AddContestForm, SubmitForm, AddTopicForm, ReplyForm, PasswordForm
from .. import db
from ..models import User, Contest, Problem, Contest_problem, Submission, Topic, Reply, Permission
import datetime
import time
from ..decorators import mod_required, admin_required
from datetime import datetime
import json

def check_contest_access(cid):
    # print "is_admin", current_user.is_administrator
    if current_user.is_administrator:
        return True
    contest = Contest.query.filter_by(id=cid).first()
    if contest.user == current_user:
        return True

    if contest.visible == False and not current_user.can(Permission.PUBLISH_NEWS):
        return False

    if contest.type == 0:
        return True

    if not current_user.have_access_to(1, cid):
        return False


@contest.route('/passwd/<cid>', methods=['GET', 'POST'])
def password(cid):
    print "to password"
    contest = Contest.query.filter_by(id=cid).first()
    form = PasswordForm()
    if form.validate_on_submit():
        if form.password.data == contest.password:
            cstr = 'contest' + str(cid)
            session[cstr] = True
            return redirect(url_for('contest.show', cid = cid))
        else:
            return render_template('contest/need_password.html', contest=contest, form=form)
    return render_template('contest/need_password.html', contest=contest, form=form)


@contest.route('/nopermission/<cid>', methods=['GET', 'POST'])
def nopermission(cid):
    contest = Contest.query.filter_by(id=cid).first()
    # form = PasswordForm()
    # if form.validate_on_submit():
    #     if form.password.data == contest.password:
    #         cstr = 'contest' + str(cid)
    #         session[cstr] = True
    #         return redirect(url_for('contest.show', cid = cid))
    #     else:
    #         return render_template('contest/need_password.html', contest=contest, form=form)
    return render_template('contest/nopermission.html', contest=contest)



@contest.route('/', methods=['GET', 'POST'])
def list():
    page = request.args.get('page', 1, type=int)

    if current_user.can(Permission.PUBLISH_NEWS):
        pagination = Contest.query.order_by(Contest.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    else:
        pagination = Contest.query.filter_by(visible=True).order_by(Contest.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )

    contests = pagination.items
    return render_template('contest/list.html', contests=contests, pagination=pagination)



@contest.route('/search', methods=['GET', 'POST'])
def search():
    page = request.args.get('page', 1, type=int)
    key = request.args.get('key', '')
    type = request.args.get('type', '')
    print "key == ", key, "  ", "type == ", type
    if key == None or key == "":
        return redirect(url_for("contest.list"))
    if type == None or type == "":
        return redirect(url_for("contest.list"))
    if int(type) == 0:
        pagination = Contest.query.filter(Contest.title.like("%" + key + "%")).order_by(Contest.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    else :
        user = User.query.filter_by(nickname=key).first()
        if user == None:
            return redirect(url_for("contest.list"))

        pagination = Contest.query.filter_by(user=user).order_by(Contest.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
        # print pagination.items

    contests = pagination.items
    return render_template('contest/search_list.html', contests=contests, pagination=pagination)





@contest.route('/add_contest', methods=['GET', 'POST'])
@login_required
@admin_required
def add_contest():
    form = AddContestForm()
    # print "start :", form.start_time.data
    # print "end :", form.end_time.data
    # if form.is_submitted():
    #     print "submitted"
    # if form.validate():
    #     print "valid"
    # print form.errors
    # print "start :", form.start_time.data
    # print "end :", form.end_time.data
    # form.problem_list.data = "{'Singleproblem':[{'id':1000, 'title':'xxxxx'}, {'id':1001, 'title':'yyyyy'}]}";
    # print "start time == ", form.start_time.data
    if form.validate_on_submit():
        print form.problem_list.data
        problems = json.loads(form.problem_list.data)
        # print int(problems[0]['id'])
        contest = Contest(title=form.title.data, start_time=form.start_time.data, end_time=form.end_time.data,
                          type=form.type.data, password=form.password.data, visible=form.visible.data,
                          user=current_user)
        db.session.add(contest)
        db.session.commit()

        pos = 1
        for data in problems:
            problem = Problem.query.filter_by(id=int(data['id'])).first()
            if problem == None:
                break
            title = data['name']
            if title == None:
                title = problem.title
            cp = Contest_problem(problem=problem, contest=contest, title=title, id=pos)
            pos += 1
            db.session.add(cp)
            db.session.commit()


        return redirect(url_for('contest.show', cid=contest.id))
    form.start_time.data = datetime.now()
    form.end_time.data = datetime.now()
    return render_template('contest/add_contest.html', form=form)


@contest.route('/<cid>', methods=['GET', 'POST'])
@login_required
# @can_access
def show(cid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))
    # current_user.have_access_to2(cid)
    contest = Contest.query.filter_by(id=cid).first()
    return render_template('contest/contest.html', contest=contest)



@contest.route('/<cid>/problem/<pid>', methods=['GET', 'POST'])
def show_problem(cid, pid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))
    # problem = Problem.query.filter_by(id=id).first()
    # return render_template('contest/problem.html',)
    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    cb = Contest_problem.query.filter_by(cid=cid).filter_by(id=pid).first()
    form = SubmitForm()
    problem = Problem.query.filter_by(id=cb.pid).first()
    now = datetime.now()
    if form.validate_on_submit():
        if not current_user.is_authenticated:
            flash('please login first')
            return render_template('contest/problem.html', cb=cb, form=form, contest=contest)

        if now > contest.end_time:
            flash('contest end')
            return render_template('contest/problem.html', cb=cb, form=form, contest=contest, problem=problem)

        if now < contest.start_time:
            flash('contest not start')
            return render_template('contest/problem.html', cb=cb, form=form, contest=contest, problem=problem)
        problem.total_submited += 1
        current_user.total_submit += 1
        submission = Submission(language=form.language.data, code=form.code.data,
                                contest_id=cid,
                                problem=problem,
                                shared=False,
                                user=current_user,
                                code_length=len(form.code.data)
        )
        db.session.add(submission)
        db.session.commit()
        return redirect(url_for('contest.status', cid=cid))
    return render_template('contest/problem.html', cb=cb, form=form, contest=contest, problem=problem)



@contest.route('/<cid>/status', methods=['GET', 'POST'])
def status(cid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))
    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    page = request.args.get('page', 1, type=int)
    pagination = Submission.query.filter_by(contest_id=cid).order_by(Submission.id.desc()).paginate(
        page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
        error_out=False
    )
    submissions = pagination.items
    # print submissions
    #newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('contest/status.html', submissions=submissions, pagination=pagination, contest=contest)



@contest.route('/<cid>/topics', methods=['GET', 'POST'])
def topics(cid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))

    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    # topics = contest.topics
    return render_template('contest/topics.html', contest=contest)



# id = db.Column(db.Integer, primary_key=True)
#     title = db.Column(db.String(64))
#     content = db.Column(db.Text())
#     create_time = db.Column(db.DateTime, default=datetime.utcnow, index=True)
#     user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
#     contest_id = db.Column(db.Integer, db.ForeignKey('contest.id'))
#     problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
#     is_delete = db.Column(db.Boolean, default=False)
#     replys = db.relationship('Reply', backref='topic')

@contest.route('/<cid>/add_topic', methods=['GET', 'POST'])
def add_topic(cid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))
    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    form = AddTopicForm()

    print "form problem data = ", form.problem.data

    if form.validate_on_submit():
        cb = Contest_problem.query.filter_by(cid=cid).filter_by(id=form.problem.data).first()
        if cb is None:
            abort(404)
        problem = Problem.query.filter_by(id=cb.pid).first()
        # problem = Problem.query.filter_by(id=form.problem.data).first()
        topic = Topic(title=form.title.data, content=form.content.data, contest=contest,
                        problem=problem,
                        is_delete=False, user=current_user, contest_problem_id=form.problem.data)
        db.session.add(topic)
        db.session.commit()
        return redirect(url_for('contest.topics', cid=cid))
    return render_template('contest/add_topic.html', contest=contest, form=form)


    # id = db.Column(db.Integer, primary_key=True)
    # content = db.Column(db.Text())
    # create_time = db.Column(db.DateTime, default=datetime.utcnow, index=True)
    # user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    # is_delete = db.Column(db.Boolean, default=False)
    # topic_id = db.Column(db.Integer, db.ForeignKey('topic.id'))
    # news_id =  db.Column(db.Integer, db.ForeignKey('news.id'))

@contest.route('/<cid>/topic/<tid>', methods=['GET', 'POST'])
def show_topic(cid, tid):
    flag = check_contest_access(cid)
    if flag == False:
        return redirect(url_for('contest.nopermission', cid=cid))
    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    topic = Topic.query.filter_by(id=tid).first()
    replys = topic.replys
    form = ReplyForm()
    if form.validate_on_submit():
        if not current_user.can(Permission.REPLY):
            flash("no permissions")
            return render_template('contest/show_topic.html', contest=contest, topic=topic, replys=replys, form=form)
        reply = Reply(content=form.content.data, user=current_user, is_delete=0, topic=topic)
        db.session.add(reply)
        db.session.commit()
        return redirect(url_for('contest.show_topic', cid=contest.id, tid=topic.id))
    return render_template('contest/show_topic.html', contest=contest, topic=topic, replys=replys, form=form)



class UR:
    def __init__(self, uid, nickname):
        self.uid = uid
        self.nickname = nickname
        self.solved = 0
        self.time = 0
        self.ac_time = {}
        self.wa_count = {}

    def update(self, pid, time, res):
        if pid in self.ac_time and self.ac_time[pid] > 0:
            return
        if res == 2 :
            self.ac_time[pid] = time
            self.solved += 1
            if pid not in self.wa_count:
                self.wa_count[pid] = 0
            self.time += time + self.wa_count[pid] * 1200
        else:
            # print self.wa_count
            if pid in self.wa_count:
                self.wa_count[pid] += 1
            else:
                self.wa_count[pid] = 1


# def datetimeformat(value):
#     return str(datetime.timedelta(seconds=value))


@contest.route('/<cid>/rank', methods=['GET', 'POST'])
def rank(cid):
    contest = Contest.query.filter_by(id=cid).first()

    if contest.status == 0:
        return redirect(url_for('contest.show', cid=contest.id))

    data = db.session.execute('select users.id, users.nickname, submission.result, submission.create_time, contest_problem.id from (select * from submission where submission.contest_id=:cid) submission left join users on submission.user_id = users.id left join contest_problem on contest_problem.pid=submission.problem_id and contest_problem.cid=submission.contest_id order by users.id, submission.create_time', {'cid':cid}).fetchall()
    # print data
    user_list = []
    index = -1
    pre_id = -1
    for single in data:
        if pre_id != single[0]:
            index += 1

            user_list.append(UR(single[0], single[1]))
            pre_id = single[0]
        # print time.mktime(contest.start_time)
        # print time.strptime(contest.start_time, "%Y-%m-%d %H:%M:%S")
        user_list[index].update(single[4], (single[3] - contest.start_time).seconds, single[2])

    # print user_list[0].wa_count
    return render_template('contest/rank.html', contest=contest, user_data=user_list)



@contest.route('/<cid>/static', methods=['GET', 'POST'])
def static(cid):
    check_contest_access(cid)
    pass



