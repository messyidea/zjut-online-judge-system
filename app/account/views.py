from flask import render_template, redirect, request, url_for, flash
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import account
from .. import db
from ..models import User,News
from .forms import LoginForm, RegistrationForm, ProfileForm


@account.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('main.index')
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('main.index'))
        flash('Invalid username or password')
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('account/login.html', form=form, newslist=newslist)


@account.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data, username=form.username.data, password=form.password.data)
        db.session.add(user)
        flash('You can login now')
        return redirect(url_for('account.login'))
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('account/register.html', form=form, newslist=newslist)


@account.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))

@account.route('/setting', methods=['GET', 'POST'])
@login_required
def setting():
    form = ProfileForm()
    if form.validate_on_submit():
        #print "aboutme  ",form.about_me.data
        user = current_user
        user.email = form.email.data
        user.about_me = form.about_me.data
        user.gender = form.gender.data
        #print "gender",form.gender.data
        user.address = form.address.data
        user.college = form.college.data
        user.default_language = form.college.data
        user.grade = form.grade.data
        user.major = form.major.data
        user.nickname = form.nickname.data
        user.phone = form.phone.data
        user.school = form.school.data
        user.uclass = form.uclass.data
        user.blog = form.blog.data
        user.qq =form.qq.data
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('account.setting'))
    form.username.data = current_user.username
    form.about_me.data = current_user.about_me
    form.address.data = current_user.address
    form.college.data = current_user.college
    form.default_language.data = current_user.default_language
    form.grade.data = current_user.grade
    form.major.data = current_user.major
    form.nickname.data = current_user.nickname
    form.phone.data = current_user.phone
    form.school.data = current_user.school
    form.uclass.data = current_user.uclass
    form.qq.data = current_user.qq
    form.email.data = current_user.email
    form.blog.data = current_user.blog
    form.gender.data = current_user.gender
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return  render_template('account/setting.html', form=form, newslist=newslist)


@account.route('/<id>', methods=['GET', 'POST'])
def show(id):
    user = User.query.filter_by(id=id).first()
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('account/user.html', user=user, newslist=newslist)





