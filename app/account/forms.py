from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField, SelectField
from wtforms.validators import Required, Length, Email, DataRequired, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User, Gender

class LoginForm(Form):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Login in')

class RegistrationForm(Form):
    username = StringField('Username', validators=[DataRequired(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0, 'Usernames must have only letters, numbers, dots or underscores')])
    email = StringField('Email', validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('password2', message='Passwords mush match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already registered')


class ProfileForm(Form):
    username = StringField('Username')
    email = StringField('Email')
    default_language = StringField('Default language')
    gender = SelectField('Gender', coerce=int, choices=Gender.CHOICES)
    school = StringField('School')
    college = StringField('College')
    major = StringField('Major')
    grade = StringField('Grade')
    uclass = StringField('Class')
    qq = StringField('QQ')
    blog = StringField('Blog')
    phone = StringField('Phone')
    address = StringField('Address')
    nickname = StringField('Nickname')
    about_me = StringField('About me')
    submit = SubmitField('Save')
