from flask import render_template, redirect, request, url_for, flash, current_app
from flask.ext.login import current_user
from . import status
from .. import db
from ..models import User, Submission, News


@status.route('/', methods=['GET', 'POST'])
def list():
    page = request.args.get('page', 1, type=int)
    pagination = Submission.query.order_by(Submission.create_time.desc()).paginate(
        page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
        error_out=False
    )
    submissions = pagination.items
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('status/list.html', submissions=submissions, pagination=pagination, newslist=newslist)


@status.route('/search', methods=['GET', 'POST'])
def search():
    page = request.args.get('page', 1, type=int)
    problem_id = request.args.get('problem_id', 0, type=int)
    author = request.args.get('author', '')
    language = request.args.get('language', 0, type=int)
    status = request.args.get('status', 0, type=int)
    print "problem_id == ", problem_id
    pagination = Submission.query
    if problem_id != 0:
        print 111111
        pagination = pagination.filter_by(problem_id=problem_id)
    if (author != None) and (author != ''):
        print 22222
        user = User.query.filter_by(username=author).first()
        pagination = pagination.filter_by(user=user)
    if language != 0:
        print 33333
        pagination = pagination.filter_by(language=language-1)
    if status != 0:
        print 44444
        pagination = pagination.filter_by(result=status-1)

    pagination = pagination.order_by(Submission.create_time.desc()).paginate(
        page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
        error_out=False
    )
    submissions = pagination.items
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('status/search_list.html', submissions=submissions, pagination=pagination, newslist=newslist)