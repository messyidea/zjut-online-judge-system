from functools import wraps
from flask import abort
from flask.ext.login import current_user
from .models import Permission

def permission_required(permission):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can(permission):
                abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator

def admin_required(f):
    return permission_required(Permission.ADMINISTER)(f)

def mod_required(f):
    return permission_required(Permission.PUBLISH_NEWS | Permission.PUBLISH_PROBLEM)(f)

# def access_required():
#     def decorator(f):
#         @wraps(f)
#         def decorated_function(*args, **kwargs):
#             if not current_user.have_access_to(kwargs['cid']):
#                 abort(403)
#             return f(*args, **kwargs)
#         return decorated_function
#     return decorator
#
# def can_access(f):
#     return access_required()(f)

# def can_access(cid):
#     def decorator(func):
#         @wraps(func)
#         def wrapper(*args, **kw):
#             # print '%s %s():' % (text, func.__name__)
#             if not current_user.have_access_to(cid):
#                 abort(403)
#             return func(*args, **kw)
#         return wrapper
#     return decorator

