from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.mail import Mail
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
#from flask.ext.pagedown import PageDowm
from config import config

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()
#pagedown = PageDowm()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'account.login'

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    #pagedown.init_app(app)
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .problem import problem as problem_blueprint
    app.register_blueprint(problem_blueprint, url_prefix='/problem')

    from .status import status as status_blueprint
    app.register_blueprint(status_blueprint, url_prefix='/status')

    from .ranks import ranks as ranks_blueprint
    app.register_blueprint(ranks_blueprint, url_prefix='/ranks')

    from .dashboard import dashboard as dashboard_blueprint
    app.register_blueprint(dashboard_blueprint, url_prefix='/dashboard')

    from .contest import contest as contest_blueprint
    app.register_blueprint(contest_blueprint, url_prefix='/contest')

    from .news import news as news_blueprint
    app.register_blueprint(news_blueprint, url_prefix='/news')

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    return app