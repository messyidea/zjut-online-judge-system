from flask import render_template, redirect, request, url_for, flash
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import dashboard
from .. import db
from ..models import User, News, Problem
from .forms import AddProblemForm, AddNewsForm, LoadProblemForm, NewsEditForm
from ..decorators import mod_required, admin_required
from sqlalchemy import func

@dashboard.route('/add_problem', methods=['GET', 'POST'])
@mod_required
def add_problem():
    if not current_user.is_authenticated:
        flash('no permission')
        return redirect('main.index')
    form = AddProblemForm()
    # form2 = LoadProblemForm()
    # new_or_change = False
    tid = 0
    # print "data == ", form.id.data
    if form.validate_on_submit():
        if form.id.data != None and form.id.data != "":
            # change
            tid = form.id.data
            problem = Problem.query.filter_by(id = form.id.data).first()
            problem.title = form.title.data
            problem.pro_desc = form.pro_desc.data
            problem.difficulty = form.difficulty.data
            problem.hint = form.hint.data
            problem.memory_limit = form.memory_limit.data
            problem.time_limit = form.time_limit.data
            problem.pro_input = form.pro_input.data
            problem.pro_output = form.pro_output.data
            problem.sample_input = form.sample_input.data
            problem.sample_output = form.sample_output.data
            problem.source = form.source.data
            problem.visible = form.visible.data
            problem.spj = form.spj.data

            db.session.add(problem)
            db.session.commit()

        else:
            # new
            tp = Problem.query.order_by(Problem.id.desc()).first()
            if(tp == None):
                tid = 1000
            else:
                tid = tp.id + 1

            problem = Problem(title = form.title.data,
                              id = tid,
                              time_limit = form.time_limit.data,
                              memory_limit = form.memory_limit.data,
                              pro_desc = form.pro_desc.data,
                              pro_input = form.pro_input.data,
                              pro_output = form.pro_output.data,
                              sample_input = form.sample_input.data.strip(),
                              sample_output = form.sample_output.data.strip(),
                              hint = form.hint.data,
                              source = form.source.data,
                              visible = form.visible.data,
                              difficulty = form.difficulty.data,
                              spj = form.spj.data,
                              user = current_user)

            db.session.add(problem)
            db.session.commit()

        # print "diff = ",form.difficulty.data
        return redirect(url_for('problem.show', id=tid))

    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('dashboard/add_problem.html', form=form, newslist=newslist)



@dashboard.route('/edit/<id>', methods=['GET', 'POST'])
@mod_required
def edit_problem(id):
    problem = Problem.query.filter_by(id=id).first()
    #print "pro == ",problem
    if problem == None:
        flash("none user!")
        return redirect(url_for('dashboard.add_problem'))
    # form2 = LoadProblemForm()

    # form2.id.data = id
    form = AddProblemForm()
    # load
    form.id.data = id
    form.title.data = problem.title
    form.pro_desc.data = problem.pro_desc
    form.difficulty.data = problem.difficulty
    form.hint.data = problem.hint
    form.memory_limit.data = problem.memory_limit
    form.time_limit.data = problem.time_limit
    form.pro_input.data = problem.pro_input
    form.pro_output.data = problem.pro_output
    form.sample_input.data = problem.sample_input
    form.sample_output.data = problem.sample_output
    form.source.data = problem.source
    form.visible.data = problem.visible
    form.spj.data = problem.spj
    #print "diff == ",problem.difficulty
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('dashboard/edit_problem.html', form=form, newslist=newslist, problem=problem)




@dashboard.route('/add_news', methods=['GET', 'POST'])
@mod_required
def add_news():
    if not current_user.is_authenticated:
        flash('no permission')
        return redirect('main.index')
    form = AddNewsForm()
    if form.validate_on_submit():
        news = News(title=form.title.data, content=form.content.data, user=current_user, is_show=form.is_show.data)
        db.session.add(news)
        db.session.commit()
        #print "id == ",news.id
        return redirect(url_for('news.show', news_id=news.id))
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('dashboard/add_news.html', form=form, newslist=newslist)


@dashboard.route('/edit_news/<id>', methods=['GET', 'POST'])
@mod_required
def edit_news(id):
    news = News.query.filter_by(id=id).first()
    form = NewsEditForm()
    if form.validate_on_submit():
        news.title = form.title.data
        news.content = form.content.data
        news.is_delete = form.is_delete.data
        news.is_show = form.is_show.data
        db.session.add(news)
        db.session.commit()
        return redirect(url_for('news.show', news_id=news.id))
    form.title.data = news.title
    form.content.data = news.content
    form.is_delete.data = news.is_delete
    form.is_show.data = news.is_show
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('dashboard/edit_news.html', form=form, newslist=newslist, news=news)



@dashboard.route('/user_manage', methods=['GET', 'POST'])
@admin_required
def user_manage():
    return render_template('dashboard/user_manage.html')





@dashboard.route('/dashboard', methods=['GET', 'POST'])
@mod_required
def dashboard():
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('dashboard/dashboard.html', newslist=newslist)



