from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField, IntegerField, TextAreaField, HiddenField, SelectField
from wtforms.validators import Required, Length, Email, DataRequired, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User, Difficulty

class AddProblemForm(Form):
    id = HiddenField('PID')
    title = StringField('Title')
    time_limit = IntegerField('Time Limit')
    memory_limit = IntegerField('Memory Limit')
    pro_desc = HiddenField('Problem desc')
    pro_input = TextAreaField('Input')
    pro_output = TextAreaField('Output')
    sample_input = TextAreaField('sample input')
    sample_output = TextAreaField('sample output')
    hint = TextAreaField('Hint')
    source = StringField('Source')
    spj = BooleanField('SPJ')
    visible = BooleanField('Visible')
    difficulty = SelectField('Difficulty', coerce=int, choices=Difficulty.CHOICES)
    submit = SubmitField('Submit')


class AddNewsForm(Form):
    title = StringField('Title')
    content = HiddenField('Content')
    is_show = BooleanField('is_show')
    submit = SubmitField('Add')

class LoadProblemForm(Form):
    id = IntegerField('PID')
    submit = SubmitField('Submit')


class NewsEditForm(Form):
    title = StringField('Title')
    content = TextAreaField('content')
    is_show = BooleanField('is_show')
    is_delete = BooleanField('is_delete')
    submit = SubmitField('update')



