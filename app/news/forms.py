from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, BooleanField
from wtforms.validators import Required

class NameForm(Form):
    name = StringField('what is your name', validators=[Required()])
    submit = SubmitField('Submit')

class ReplyForm(Form):
    content = TextAreaField('Content')
    submit = SubmitField('Submit')

