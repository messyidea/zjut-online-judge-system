#from datetime import datetime
from flask import render_template, session, redirect, url_for, current_app, request, flash
from flask.ext.login import login_user, logout_user, login_required, current_user
from .. import db
from ..models import User, News, Reply, Permission
from . import news
from .forms import ReplyForm


@news.route('/', methods=['GET', 'POST'])
def list():
    #return redirect(url_for('.index'))
    #form = NameForm()
    #return render_template('index.html', form=form, name=session.get('name'))
    page = request.args.get('page', 1, type=int)
    pagination = News.query.filter_by(is_delete=False).order_by(News.id.desc()).paginate(
        page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
        error_out=False
    )
    news = pagination.items
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('news/list.html', news=news, pagination=pagination, newslist=newslist)


@news.route('/<news_id>', methods=['GET', 'POST'])
def show(news_id):
    news = News.query.filter_by(id=news_id).first()
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    form = ReplyForm()
    if form.validate_on_submit():
        if not current_user.is_authenticated:
            flash('please login first')
            return render_template('news/news.html', news=news, form=form, newslist=newslist)
        if not current_user.can(Permission.REPLY):
            flash('no permissions')
            return render_template('news/news.html', news=news, form=form, newslist=newslist)
        reply = Reply(content=form.content.data, user=current_user, news=news)
        db.session.add(reply)
        db.session.commit()
        return redirect(url_for('news.show', news_id=news.id))
    return render_template('news/news.html', news=news, form=form, newslist=newslist)



