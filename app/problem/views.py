from flask import render_template, redirect, request, url_for, flash, current_app, abort
from flask.ext.login import login_required, current_user
from . import problem
from .. import db
from ..models import User, Problem, Submission, Language, News, Topic, Reply, Permission
from .forms import SubmitForm, ReplyForm, AddTopicForm




@problem.route('/', methods=['GET', 'POST'])
def list():
    page = request.args.get('page', 1, type=int)
    if current_user.can(Permission.PUBLISH_PROBLEM):
        pagination = Problem.query.order_by(Problem.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    else:
        pagination = Problem.query.filter_by(visible=True).order_by(Problem.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    problems = pagination.items
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('problem/list.html', problems=problems, pagination=pagination, newslist=newslist)





@problem.route('/search', methods=['GET', 'POST'])
def search():
    page = request.args.get('page', 1, type=int)
    key = request.args.get('key', '')
    type = request.args.get('type', '')
    print "key == ", key, "  ", "type == ", type
    if key == None or key == "":
        return redirect(url_for("problem.list"))
    if type == None or type == "":
        return redirect(url_for("problem.list"))
    if int(type) == 0:
        pagination = Problem.query.filter(Problem.id==int(key)).filter_by(visible=True).order_by(Problem.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    else :
        pagination = Problem.query.filter(Problem.title.like("%" + key + "%")).filter_by(visible=True).order_by(Problem.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )

    problems = pagination.items
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('problem/search_list.html', problems=problems, pagination=pagination, newslist=newslist)



@problem.route('/<id>', methods=['GET', 'POST'])
def show(id):
    problem = Problem.query.filter_by(id=id).first()
    if problem.visible == False and (not current_user.can(Permission.PUBLISH_PROBLEM)):
        return render_template('no_permission.html')
    form = SubmitForm()
    #form.language.data = current_user.default_language
    if form.validate_on_submit():
        #submission = Submission()
        # print "language:", form.language.data
        if not current_user.is_authenticated:
            flash('please login first')
            return render_template('problem/problem.html', problem=problem, form=form)
        # print form.code.data
        # print "len == ", len(form.code.data)
        submission = Submission(language=form.language.data, code=form.code.data,
                                problem=Problem.query.filter_by(id=id).first(),
                                shared=False, code_length=len(form.code.data),
                                user=current_user
        )
        db.session.add(submission)
        db.session.commit()
        current_user.total_submit = current_user.total_submit + 1
        problem.total_submited = problem.total_submited + 1

        return redirect(url_for('status.list'))
    #form.language.data = 0
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('problem/problem.html', problem=problem, form=form, newslist=newslist)



@problem.route('/code/<id>', methods=['GET', 'POST'])
def show_code(id):
    submission = Submission.query.filter_by(id=id).first()
    if (not current_user.is_administrator) and (submission.user != current_user):
        abort(403)

    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('status/code.html', submission=submission, newslist=newslist)


@problem.route('/<id>/discuss', methods=['GET', 'POST'])
def show_topics(id):
    problem = Problem.query.filter_by(id=id).first()
    # topics = problem.topics
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    return render_template('problem/topics.html', problem=problem, newslist=newslist)



@problem.route('/<id>/add_topic', methods=['GET', 'POST'])
def add_topic(id):
    problem = Problem.query.filter_by(id=id).first()
    form = AddTopicForm()
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    if form.validate_on_submit():
        problem = Problem.query.filter_by(id=form.problem.data).first()
        if not current_user.is_authenticated:
            flash('please login first')
            return render_template('problem/add_topic.html', form=form, problem=problem, newslist=newslist)
        topic = Topic(title=form.title.data, content=form.content.data,
                        problem=problem, user=current_user)
        db.session.add(topic)
        db.session.commit()
        return redirect(url_for('problem.show_topic', id=problem.id, tid=topic.id))
    return render_template('problem/add_topic.html', form=form, problem=problem, newslist=newslist)



@problem.route('/<id>/discuss/<tid>', methods=['GET', 'POST'])
def show_topic(id, tid):
    problem = Problem.query.filter_by(id=id).first()
    topic = Topic.query.filter_by(id=tid).first()
    print topic
    # topics = problem.topics
    newslist = News.query.filter_by(is_show=True).filter_by(is_delete=False).order_by(News.id.desc())
    form = ReplyForm()
    if form.validate_on_submit():
        if not current_user.is_authenticated:
            flash('please login first')
            return render_template('problem/topic.html', problem=problem, topic=topic, newslist=newslist, form=form)
        if not current_user.can(Permission.REPLY):
            flash("no permissions")
            return render_template('problem/topic.html', problem=problem, topic=topic, newslist=newslist, form=form)
        reply = Reply(content=form.content.data, user=current_user, is_delete=0, topic=topic)
        db.session.add(reply)
        db.session.commit()
        return redirect(url_for('problem.show_topic', id=problem.id, tid=topic.id))
    return render_template('problem/topic.html', problem=problem, topic=topic, newslist=newslist, form=form)