from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, TextField, SelectField, HiddenField, IntegerField
from wtforms.validators import DataRequired, Length
from wtforms import ValidationError
from ..models import Language


class SubmitForm(Form):
    language = SelectField('Language', coerce=int, choices=Language.CHOICES)
    code = HiddenField('Code')
    submit = SubmitField('Submit')


class AddTopicForm(Form):
    problem =IntegerField('Problem')
    title = StringField('Title')
    content = TextAreaField('Content')
    submit = SubmitField('Submit')


class ReplyForm(Form):
    content = TextAreaField('Content')
    submit = SubmitField('Submit')