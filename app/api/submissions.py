from flask import jsonify, request, current_app, url_for
from . import api
from ..models import User, Contest, Contest_problem, Privilege, Problem, Submission
from ..decorators import mod_required, admin_required
from ..util import pagination_to_json, message_to_json, item_to_json, items_to_json
from .. import db

@api.route('/submissions/', methods=['GET'])
@mod_required
def get_submissions():
    page = request.args.get('page', 1, type=int)
    pagination = Submission.query.order_by(Submission.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )

    return pagination_to_json(pagination)


@api.route('/submissions/rejudge', methods=['PUT'])
@mod_required
def rejudge_submission():
    id = request.args.get('id', -1, type=int)
    if id == -1:
        return message_to_json(1, "missing id")

    submission = Submission.query.filter_by(id=id).first()
    if submission == None:
        return message_to_json(1, "can not find submission")

    submission.result = 0
    db.session.add(submission)
    db.session.commit()

    return message_to_json(0, "rejudge ok")



#
# @api.route('/contests/getcontest', methods=['GET'])
# @admin_required
# def get_contest():
#     contest_id = request.args.get('contest_id', -1, type=int)
#     if contest_id == -1:
#         return message_to_json(1, "missing contest id")
#     contest = Contest.query.filter_by(id=contest_id).first()
#     if contest == None:
#         return message_to_json(1, "contest not found")
#
#     return item_to_json(contest)
#
# @api.route('/contests/getcontestproblem', methods=['GET'])
# @admin_required
# def get_contest_problem():
#     contest_id = request.args.get('contest_id', -1, type=int)
#     if contest_id == -1:
#         return message_to_json(1, "missing contest id")
#     contest = Contest.query.filter_by(id=contest_id).first()
#     if contest == None:
#         return message_to_json(1, "contest not found")
#     cbs = Contest_problem.query.filter_by(contest=contest).all()
#     return items_to_json(cbs)
#
#
# @api.route('/contests/getcontestusers', methods=['GET'])
# @admin_required
# def get_contest_users():
#     contest_id = request.args.get('contest_id', -1, type=int)
#     if contest_id == -1:
#         return message_to_json(1, "missing contest id")
#     contest = Contest.query.filter_by(id=contest_id).first()
#     if contest == None:
#         return message_to_json(1, "contest not found")
#     key = (1 << 24) + int(contest.id)
#     # print "key == ", key
#     pr = Privilege.query.filter_by(key=key).all()
#
#     ans = []
#     for spr in pr:
#         # print "user id == ", spr.user_id
#         user = User.query.filter_by(id=spr.user_id).first()
#         if user == None:
#             continue
#         ans.append(user.username)
#     if ans == []:
#         # print "left message none"
#         return message_to_json(0, "")
#     # print "ans == ", ans
#     rst = ",".join(ans)
#     # print "rst == ", rst
#     # print "rst == ", rst
#     if rst == None:
#         # print "return message none"
#         return message_to_json(0, "")
#
#     return message_to_json(0, rst)
#
#
# @api.route('/contests/update_contest', methods=['PUT'])
# @admin_required
# def update_contest():
#     # print request.json
#     # form = request.form
#     # print request.json['problems']
#     id = request.json.get('id')
#     if id == None:
#         return message_to_json(1, "missing contest id")
#
#     problems = request.json.get('problems')
#     if problems == None:
#         return message_to_json(1, "missing problems")
#
#     contest = Contest.query.filter_by(id=id).first()
#     if contest == None:
#         return message_to_json(1, "contest not found")
#
#     if request.json.get("title") != None:
#         contest.title = request.json.get("title")
#     if request.json.get("start_time") != None:
#         contest.start_time = request.json.get("start_time")
#     if request.json.get("end_time") != None:
#         contest.ent_time = request.json.get("end_time")
#     if request.json.get("visible") != None:
#         contest.visible = request.json.get("visible")
#     if request.json.get("type") != None:
#         contest.type = request.json.get("type")
#
#     db.session.add(contest)
#     db.session.commit()
#
#     # clear contest_problem
#     pros = Contest_problem.query.filter_by(cid=id).all()
#     for pro in pros:
#         db.session.delete(pro)
#
#     db.session.commit()
#
#     # add contest_problems
#     pos = 1
#     for pro in problems:
#         pid = pro.get('id')
#         if pid == None:
#             continue
#         problem = Problem.query.filter_by(id=pid).first()
#         if problem == None:
#             continue
#         title = pro.get('title')
#         if title == None:
#             title = problem.title
#         cp = Contest_problem(cid=id, pid=pid, title=title, id=pos)
#         tcp = Contest_problem.query.filter_by(cid=id).filter_by(pid=pid).first()
#         if tcp != None:
#             continue
#         db.session.add(cp)
#         db.session.commit()
#         pos += 1
#
#
#     # clear pri
#     key = (1 << 24) + int(contest.id)
#     pris = Privilege.query.filter_by(key=key).all()
#     for pri in pris:
#         db.session.delete(pri)
#     db.session.commit()
#
#     # add pri
#     userliststr = request.json.get("contest_users")
#     userliststr = userliststr.replace(" ", "")
#     userlist = userliststr.split(',')
#     for username in userlist:
#         print "username == ", "'", username , "'"
#         user = User.query.filter_by(username=username).first()
#         if user == None:
#             continue
#         tupri = Privilege.query.filter_by(user_id=user.id).filter_by(key=key).first()
#         if tupri != None:
#             continue
#         print "add user"
#         upri = Privilege(user_id=user.id, key=key)
#         db.session.add(upri)
#         db.session.commit()
#
#
#     return message_to_json(0, "ok")