from flask import jsonify, request, current_app, url_for
from . import api
from ..models import User
from ..decorators import mod_required, admin_required
from ..util import pagination_to_json, message_to_json
from .. import db

@api.route('/users/', methods=['GET'])
@admin_required
def get_users():
    page = request.args.get('page', 1, type=int)
    pagination = User.query.order_by(User.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    # pagination = User.query.order_by(User.id.asc()).paginate(
    #         page, per_page=2,
    #         error_out=False
    #     )
    return pagination_to_json(pagination)
    # users = User.query.all()
    # return jsonify({'users': [user.to_json() for user in users]})



@api.route('/users/update_user', methods=['put'])
@admin_required
def edit_users():
    print "data == ", request.form
    form = request.form
    id = form.get("id")
    if id == None:
        return message_to_json(1, "error post data")

    user = User.query.filter_by(id=id).first()
    if user == None:
        return message_to_json(1, "user not found")

    username = form.get("username")
    if username != None and username != "":
        user.username = username

    password = form.get("password")
    if password != None and password != "":
        user.password = password

    role_id = form.get("role_id")
    if role_id != None and role_id != "":
        user.role_id = role_id

    email = form.get("email")
    if email != None and email != "":
        user.email = email

    db.session.add(user)
    db.session.commit()
    # print "tojson == ", user.to_json()
    return message_to_json(0)

