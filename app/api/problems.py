from flask import jsonify, request, current_app, url_for
from . import api
from ..models import User, Problem
from ..decorators import mod_required, admin_required
from ..util import pagination_to_json



@api.route('/problems/')
@mod_required
def get_problems():
    page = request.args.get('page', 1, type=int)
    pagination = Problem.query.order_by(Problem.id.asc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    return pagination_to_json(pagination)