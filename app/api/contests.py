from flask import jsonify, request, current_app, url_for, make_response, send_file
from flask.ext.login import login_user, logout_user, login_required, current_user
from . import api
from ..models import User, Contest, Contest_problem, Privilege, Problem, Submission
from ..decorators import mod_required, admin_required
from ..util import pagination_to_json, message_to_json, item_to_json, items_to_json
from .. import db

@api.route('/contests/', methods=['GET'])
@mod_required
def get_contests():
    page = request.args.get('page', 1, type=int)
    pagination = Contest.query.order_by(Contest.id.desc()).paginate(
            page, per_page=current_app.config['OJ_PROBLEMS_PER_PAGE'],
            error_out=False
        )
    return pagination_to_json(pagination)




@api.route('/contests/getcontest', methods=['GET'])
@mod_required
def get_contest():
    contest_id = request.args.get('contest_id', -1, type=int)
    if contest_id == -1:
        return message_to_json(1, "missing contest id")
    contest = Contest.query.filter_by(id=contest_id).first()
    if contest == None:
        return message_to_json(1, "contest not found")

    return item_to_json(contest)

@api.route('/contests/getcontestproblem', methods=['GET'])
@mod_required
def get_contest_problem():
    contest_id = request.args.get('contest_id', -1, type=int)
    if contest_id == -1:
        return message_to_json(1, "missing contest id")
    contest = Contest.query.filter_by(id=contest_id).first()
    if contest == None:
        return message_to_json(1, "contest not found")
    cbs = Contest_problem.query.filter_by(contest=contest).all()
    return items_to_json(cbs)


def get_result_str(result):
    if result == 0:
        return "Waiting"
    if result == 1:
        return "Runing"
    if result == 2:
        return "Accepted"
    if result == 3:
        return "Presentation Error"
    if result == 4:
        return "Wrong Answer"
    if result == 5:
        return "Runtime Error"
    if result == 6:
        return "Time Limit Exceeded"
    if result == 7:
        return "Memory Limit Exceeded"
    if result == 8:
        return "Output Limit Exceeded"
    if result == 9:
        return "Compile Error"
    return "System Error"




@api.route('/contests/download', methods=['GET'])
@mod_required
def download_code():
    contest_id = request.args.get('contest_id', -1, type=int)
    if contest_id == -1:
        return message_to_json(1, "missing contest id")
    contest = Contest.query.filter_by(id=contest_id).first()
    if contest == None:
        return message_to_json(1, "contest not found")
    # submissions = contest.submissions.all()
    submissions = Submission.query.filter_by(contest_id=contest.id).all()
    name = "contest-" + str(contest_id) + ".txt";
    filepath = "/tmp/" + name;
    f=open(filepath,'w')
    for single in submissions:
        user = single.user
        f.write("username: " + user.username + "\n")
        f.write("time:     " + str(single.create_time) + "\n")
        f.write("result:   " + str(get_result_str(single.result)) + "\n")
        f.write("code:\n")
        f.write(single.code)
        f.write("\n")
        f.write("\n")
        f.write("\n")
    f.close()
    response = make_response(send_file(filepath))
    response.headers["Content-Disposition"] = "attachment; filename=" + name + ";"
    return response



@api.route('/contests/getcontestusers', methods=['GET'])
@mod_required
def get_contest_users():
    contest_id = request.args.get('contest_id', -1, type=int)
    if contest_id == -1:
        return message_to_json(1, "missing contest id")
    contest = Contest.query.filter_by(id=contest_id).first()
    if contest == None:
        return message_to_json(1, "contest not found")
    key = (1 << 24) + int(contest.id)
    # print "key == ", key
    pr = Privilege.query.filter_by(key=key).all()

    ans = []
    for spr in pr:
        # print "user id == ", spr.user_id
        user = User.query.filter_by(id=spr.user_id).first()
        if user == None:
            continue
        ans.append(user.username)
    if ans == []:
        # print "left message none"
        return message_to_json(0, "")
    # print "ans == ", ans
    rst = ",".join(ans)
    # print "rst == ", rst
    # print "rst == ", rst
    if rst == None:
        # print "return message none"
        return message_to_json(0, "")

    return message_to_json(0, rst)


@api.route('/contests/update_contest', methods=['PUT'])
@mod_required
def update_contest():
    # print request.json
    # form = request.form
    # print request.json['problems']
    id = request.json.get('id')
    if id == None:
        return message_to_json(1, "missing contest id")

    problems = request.json.get('problems')
    if problems == None:
        return message_to_json(1, "missing problems")

    contest = Contest.query.filter_by(id=id).first()
    if contest == None:
        return message_to_json(1, "contest not found")

    if request.json.get("title") != None:
        contest.title = request.json.get("title")
    if request.json.get("start_time") != None:
        contest.start_time = request.json.get("start_time")
    if request.json.get("end_time") != None:
        contest.end_time = request.json.get("end_time")


    if request.json.get("visible") != None:
        if request.json.get("visible") == "1" or request.json.get("visible") == 1:
            contest.visible = True
        else:
            contest.visible = False

    if request.json.get("type") != None:
        if request.json.get("type") == "1" or request.json.get("type") == 1:
            contest.type = True
        else:
            contest.type = False
        # contest.type = request.json.get("type")



    db.session.add(contest)
    db.session.commit()

    # clear contest_problem
    pros = Contest_problem.query.filter_by(cid=id).all()
    for pro in pros:
        db.session.delete(pro)

    db.session.commit()

    # add contest_problems
    pos = 1
    for pro in problems:
        pid = pro.get('id')
        if pid == None:
            continue
        problem = Problem.query.filter_by(id=pid).first()
        if problem == None:
            continue
        title = pro.get('name')
        if title == None:
            title = problem.title
        cp = Contest_problem(cid=id, pid=pid, title=title, id=pos)
        tcp = Contest_problem.query.filter_by(cid=id).filter_by(pid=pid).first()
        if tcp != None:
            continue
        db.session.add(cp)
        db.session.commit()
        pos += 1


    # clear pri
    key = (1 << 24) + int(contest.id)
    pris = Privilege.query.filter_by(key=key).all()
    for pri in pris:
        db.session.delete(pri)
    db.session.commit()

    # add pri
    userliststr = request.json.get("contest_users")
    userliststr = userliststr.replace(" ", "")
    userlist = userliststr.split(',')
    for username in userlist:
        print "username == ", "'", username , "'"
        user = User.query.filter_by(username=username).first()
        if user == None:
            continue
        tupri = Privilege.query.filter_by(user_id=user.id).filter_by(key=key).first()
        if tupri != None:
            continue
        print "add user"
        upri = Privilege(user_id=user.id, key=key)
        db.session.add(upri)
        db.session.commit()


    return message_to_json(0, "ok")






@api.route('/contests/add_contest', methods=['PUT'])
@mod_required
def add_contest():
    # print request.json
    # form = request.form
    # print request.json['problems']
    title = request.json.get('title')
    if title == None:
        return message_to_json(1, "title not found")

    start_time = request.json.get('start_time')
    if start_time == None:
        return message_to_json(1, "start_time not found")

    end_time = request.json.get("end_time")
    if end_time == None:
        return message_to_json(1, "end_time not found")

    type = request.json.get("type")
    if type == None:
        return message_to_json(1, "type not found")
    if type == "1" or type == 1:
        type = True
    else:
        type = False

    password = ""

    visible = request.json.get("visible")
    if visible == None:
        return message_to_json(1, "visible not found")
    if visible == "1" or visible == 1:
        visible = True
    else:
        visible = False
    contest = Contest(title=title, start_time=start_time, end_time=end_time,
                          type=type, password=password, visible=visible,
                          user=current_user)

    db.session.add(contest)
    db.session.commit()

    id = contest.id

    problems = request.json.get('problems')
    if problems == None:
        return message_to_json(1, "missing problems")


    # add contest_problems
    pos = 1
    for pro in problems:
        pid = pro.get('id')
        if pid == None:
            continue
        problem = Problem.query.filter_by(id=pid).first()
        if problem == None:
            continue
        title = pro.get('name')
        if title == None:
            title = problem.title
        cp = Contest_problem(cid=id, pid=pid, title=title, id=pos)
        tcp = Contest_problem.query.filter_by(cid=id).filter_by(pid=pid).first()
        if tcp != None:
            continue
        db.session.add(cp)
        db.session.commit()
        pos += 1

    key = (1<<24) + int(contest.id)

    # add pri
    userliststr = request.json.get("contest_users")
    userliststr = userliststr.replace(" ", "")
    userlist = userliststr.split(',')
    for username in userlist:
        print "username == ", "'", username , "'"
        user = User.query.filter_by(username=username).first()
        if user == None:
            continue
        tupri = Privilege.query.filter_by(user_id=user.id).filter_by(key=key).first()
        if tupri != None:
            continue
        print "add user"
        upri = Privilege(user_id=user.id, key=key)
        db.session.add(upri)
        db.session.commit()


    return message_to_json(0, "ok")