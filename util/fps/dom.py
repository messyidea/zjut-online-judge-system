#!/usr/bin/python
# -*- coding: UTF-8 -*-

from xml.dom.minidom import parse
import xml.dom.minidom

DOMTree = xml.dom.minidom.parse("problem2.xml")
fps = DOMTree.documentElement

items = fps.getElementsByTagName("item")

def getText(node):
    rc = []
    nodelist = node.childNodes
    for node in nodelist:
        if node.nodeType in (node.TEXT_NODE, node.CDATA_SECTION_NODE):
            rc.append(node.data)
    return ''.join(rc)

class Problem:
    def __init__(self):
        self.title = ''
        self.time_limit = ''
        self.memory_limit = ''
        self.description = ''
        self.input = ''
        self.output = ''
        self.sample_input = ''
        self.sample_output = ''
        self.hint = ''
        self.source = ''
        self.solution = ''
        self.spj = ''

for item in items:
   nodes = item.childNodes
   p = Problem()
   for node in nodes:
        if node.nodeType == node.ELEMENT_NODE:
            # print node.nodeName
            if node.nodeName == 'title':
                p.title = getText(node)
            elif node.nodeName == 'time_limit':
                p.time_limit = getText(node)
            elif node.nodeName == 'memory_limit':
                p.memory_limit = getText(node)
            elif node.nodeName == 'description':
                p.description = getText(node)
            elif node.nodeName == 'input':
                p.input += getText(node)
            elif node.nodeName == 'output':
                p.output += getText(node)
            elif node.nodeName == 'sample_input':
                p.sample_input += getText(node)
            elif node.nodeName == 'sample_output':
                p.sample_output += getText(node)
            elif node.nodeName == 'hint':
                p.hint = getText(node)
            elif node.nodeName == 'source':
                p.source = getText(node)
            elif node.nodeName == 'solution':
                print node.getAttribute('language')
                p.solution = getText(node)
            elif node.nodeName == 'spj':
                print "spj", node.getAttribute('language')
                p.spj = getText(node)
   # print p.sample_input
