#!/usr/bin/python
# -*- coding: UTF-8 -*-

import xml.sax

class ProblemHandler( xml.sax.ContentHandler ):
   def __init__(self):
      self.CurrentData = ""
      self.title = ""
      self.time_limit = ""
      self.memory_limit = ""
      self.description = ""
      self.input1 = ""
      self.output = ""
      self.sample_input = ""
      self.sample_output = ""
      self.test_input = ""
      self.test_output = ""
      self.hint = ""
      self.source = ""
      self.solution = ""
      self.lan = ""

   # 元素开始事件处理
   def startElement(self, tag, attributes):
      self.CurrentData = tag
      if tag == "item":
         print "*****item*****"
      elif tag == "solution":
         print "language = ", attributes["language"]
      self.title = ""
      self.time_limit = ""
      self.memory_limit = ""
      self.description = ""
      self.input1 = ""
      self.output = ""
      self.sample_input = ""
      self.sample_output = ""
      self.test_input = ""
      self.test_output = ""
      self.hint = ""
      self.source = ""
      self.solution = ""
      self.lan = ""


   # 元素结束事件处理
   def endElement(self, tag):
      if self.CurrentData == "title":
         print "title:", self.title
      elif self.CurrentData == "time_limit":
         print "time_limit:", self.time_limit
      elif self.CurrentData == "memory_limit":
         print "memory_limit:", self.memory_limit
      elif self.CurrentData == "description":
         print "description:", self.description
      elif self.CurrentData == "input":
         print "input:", self.input1
      elif self.CurrentData == "output":
         print "output:", self.output
      elif self.CurrentData == "sample_input":
         print "sample_input:", self.sample_input
      elif self.CurrentData == "sample_output":
         print "sample_output:", self.sample_output
      elif self.CurrentData == "test_input":
         print "test_input:", self.test_input
      elif self.CurrentData == "test_output":
         print "test_output:", self.test_output
      elif self.CurrentData == "hint":
         print "hint:", self.hint
      elif self.CurrentData == "source":
         print "source:", self.source
      elif self.CurrentData == "solution":
         #print "lan:" self.lan, 
         print "solution:", self.solution
      self.CurrentData = ""

   # 内容事件处理
   def characters(self, content):
      if self.CurrentData == "title":
         self.title = self.title + content
      elif self.CurrentData == "time_limit":
         self.time_limit = self.time_limit + content
      elif self.CurrentData == "memory_limit":
         self.memory_limit = self.memory_limit + content
      elif self.CurrentData == "description":
         self.description = self.description + content
      elif self.CurrentData == "input":
         self.input1 = self.input1 + content
      elif self.CurrentData == "output":
         self.output = self.output + content
      elif self.CurrentData == "sample_input":
         self.sample_input = self.sample_input + content
      elif self.CurrentData == "sample_output":
         self.sample_output = self.sample_output + content
      elif self.CurrentData == "test_input":
         self.test_input = self.test_input + content
      elif self.CurrentData == "test_output":
         self.test_output = self.test_output + content
      elif self.CurrentData == "hint":
         self.hint = self.hint + content
      elif self.CurrentData == "source":
         self.source = self.source + content
      elif self.CurrentData == "solution":
         self.solution = self.solution + content
  
if ( __name__ == "__main__"):
   
   # 创建一个 XMLReader
   parser = xml.sax.make_parser()
   # turn off namepsaces
   parser.setFeature(xml.sax.handler.feature_namespaces, 0)

   # 重写 ContextHandler
   Handler = ProblemHandler()
   parser.setContentHandler( Handler )
   
   parser.parse("problem.xml")

