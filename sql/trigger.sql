create trigger update_user_solved_trigger after UPDATE
on submission FOR EACH ROW  
begin  
  
if new.result = 2 and old.result != 2 then
update problem set total_accepted = total_accepted + 1 where id = old.problem_id;
end if;

set @count = (select count(*) from submission where problem_id = old.problem_id and user_id = old.user_id and result = 2); 
  
if @count = 1 and old.result != 2 and new.result = 2 then  
update users set total_accept = total_accept + 1 where id = old.user_id;
end if;  
  
end;



#  DELIMITER $