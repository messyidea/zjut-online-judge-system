-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: oj
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('121c5d34f97a');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contest`
--

DROP TABLE IF EXISTS `contest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `contest_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contest`
--

LOCK TABLES `contest` WRITE;
/*!40000 ALTER TABLE `contest` DISABLE KEYS */;
INSERT INTO `contest` VALUES (1,'校赛',0,NULL,'2016-01-06 00:00:00','2016-01-07 00:00:00',1,1),(2,'123123123123',0,'','2016-01-22 14:01:41','2016-01-22 14:01:43',1,1),(3,'好玩',0,'sdfasdfasdf','2016-01-22 14:01:41','2016-01-22 14:01:41',1,1),(4,'蛤蛤',0,'asdf','2016-01-22 14:09:58','2016-01-22 14:09:58',1,1),(5,'嘻嘻',0,'','2016-01-22 14:23:21','2016-01-22 14:23:21',1,1),(6,'谢谢',0,'asdf','2016-01-22 14:25:17','2016-01-22 14:25:17',0,1),(7,'语言',0,'sdf','2016-01-22 14:25:17','2016-01-22 14:25:17',0,1);
/*!40000 ALTER TABLE `contest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contest_problem`
--

DROP TABLE IF EXISTS `contest_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contest_problem` (
  `pid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`,`cid`),
  KEY `cid` (`cid`),
  CONSTRAINT `contest_problem_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `contest` (`id`),
  CONSTRAINT `contest_problem_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `problem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contest_problem`
--

LOCK TABLES `contest_problem` WRITE;
/*!40000 ALTER TABLE `contest_problem` DISABLE KEYS */;
INSERT INTO `contest_problem` VALUES (1,1,'蛤蛤',NULL),(1,7,'好感觉看更好健康',1);
/*!40000 ALTER TABLE `contest_problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `is_show` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `ix_news_create_time` (`create_time`),
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'机房维护','<p><img src=\"http://127.0.0.1:5000/static/ueditor/php/upload/image/2016119/214200485265.jpg\" title=\"鼻涕虫大.jpg\" alt=\"鼻涕虫大.jpg\"/></p>','2016-01-19 13:42:02',1,0,1),(2,'xixi','<p>xixi</p>','2016-01-23 05:18:28',1,0,0);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problem`
--

DROP TABLE IF EXISTS `problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `time_limit` int(11) DEFAULT NULL,
  `memory_limit` int(11) DEFAULT NULL,
  `total_accepted` int(11) DEFAULT NULL,
  `total_submited` int(11) DEFAULT NULL,
  `pro_desc` text,
  `pro_input` text,
  `pro_output` text,
  `sample_input` text,
  `sample_output` text,
  `hint` text,
  `source` text,
  `create_time` datetime DEFAULT NULL,
  `spj` tinyint(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_problem_pid` (`pid`),
  KEY `user_id` (`user_id`),
  KEY `ix_problem_create_time` (`create_time`),
  CONSTRAINT `problem_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problem`
--

LOCK TABLES `problem` WRITE;
/*!40000 ALTER TABLE `problem` DISABLE KEYS */;
INSERT INTO `problem` VALUES (1,1000,'A+b',1000,1000,0,4,'<p>asdfasdfasdf<img src=\"http://127.0.0.1:5000/static/ueditor/php/upload/image/2016119/213951869714.gif\" title=\"7d1f6e061d950a7b4214cb590bd162d9f2d3c925.gif\" alt=\"7d1f6e061d950a7b4214cb590bd162d9f2d3c925.gif\"/></p>','asdf','adsf','asdf','adsf','asdf','asdf','2016-01-19 13:39:59',0,1,1,0);
/*!40000 ALTER TABLE `problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_id` (`news_id`),
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`),
  KEY `ix_reply_create_time` (`create_time`),
  CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`),
  CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`),
  CONSTRAINT `reply_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
INSERT INTO `reply` VALUES (1,'1111111111111111111111','2016-01-06 00:00:00',1,0,1,NULL),(2,'123123123','2016-01-23 02:46:46',1,0,1,NULL),(3,'啊是的发生地方','2016-01-23 02:46:49',1,0,1,NULL),(4,'啊是的发生地方','2016-01-23 02:46:52',1,0,1,NULL),(5,'asdf','2016-01-23 02:47:04',1,0,2,NULL),(6,'大发光火大发光火','2016-01-23 02:47:11',1,0,3,NULL),(7,'123123123','2016-01-23 03:06:34',1,0,1,NULL),(8,'收到分公司的风格','2016-01-23 03:06:38',1,0,1,NULL),(9,'收到分公司的风格啊','2016-01-23 03:06:49',1,0,1,NULL),(10,'123123123','2016-01-23 03:09:04',1,0,1,NULL),(11,'大发光火','2016-01-23 03:09:09',1,0,1,NULL),(12,'大发光火','2016-01-23 03:09:39',1,0,1,NULL),(13,'adfs','2016-01-23 04:47:39',1,0,1,NULL),(14,'xcxc','2016-01-23 04:47:46',1,0,1,NULL),(15,'hgf','2016-01-23 04:48:43',1,0,1,NULL),(16,'gjh','2016-01-23 04:49:25',1,0,1,NULL),(17,'gjh','2016-01-23 04:49:29',1,0,1,NULL),(18,'jhg','2016-01-23 04:49:43',1,0,3,NULL),(19,'adsfasdf','2016-01-23 05:05:59',1,0,5,NULL),(20,'sdfaf','2016-01-23 05:10:29',1,0,1,NULL),(21,'123123123','2016-01-23 05:18:05',1,0,NULL,1),(22,'asdfadsf','2016-01-23 05:18:08',1,0,NULL,1),(23,'fdghdgh','2016-01-23 05:18:09',1,0,NULL,1),(24,'cvbcvb','2016-01-23 05:18:17',1,0,NULL,1),(25,'asdfadsf','2016-01-23 05:18:31',1,0,NULL,2),(26,'ghjfghj','2016-01-23 05:18:33',1,0,NULL,2);
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL,
  `permissions` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `ix_roles_default` (`default`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Moderator',0,15),(2,'Administrator',0,255),(3,'User',1,3);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `code` text,
  `context_id` int(11) DEFAULT NULL,
  `info` text,
  `accepted_time` int(11) DEFAULT NULL,
  `shared` tinyint(1) DEFAULT NULL,
  `problem_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `problem_id` (`problem_id`),
  KEY `user_id` (`user_id`),
  KEY `ix_submission_accepted_time` (`accepted_time`),
  KEY `ix_submission_create_time` (`create_time`),
  CONSTRAINT `submission_ibfk_1` FOREIGN KEY (`problem_id`) REFERENCES `problem` (`id`),
  CONSTRAINT `submission_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
INSERT INTO `submission` VALUES (1,'2016-01-19 13:44:18',0,0,'收到分公司的风格收到分公司的风格',0,NULL,NULL,0,1,1),(2,'2016-01-21 08:12:53',0,0,'asdfadsf',1,NULL,NULL,0,1,1),(3,'2016-01-21 08:13:00',0,0,'sdfgsdfg',1,NULL,NULL,0,1,1),(4,'2016-01-21 08:13:11',0,0,'adsfadsfadf',0,NULL,NULL,0,1,1),(5,'2016-01-21 13:01:19',0,0,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',1,NULL,NULL,0,NULL,1),(6,'2016-01-21 13:02:14',0,0,'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb',1,NULL,NULL,0,NULL,1),(7,'2016-01-21 13:03:10',0,0,'cccccccccccccccccccccccccccccccccccccc',1,NULL,NULL,0,1,1),(8,'2016-01-21 13:42:56',0,0,'234234234',1,NULL,NULL,0,1,1),(9,'2016-01-22 06:26:37',0,0,'阿萨德发送到发送到发送到费',7,NULL,NULL,0,1,1);
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `problem_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contest_id` (`contest_id`),
  KEY `problem_id` (`problem_id`),
  KEY `user_id` (`user_id`),
  KEY `ix_topic_create_time` (`create_time`),
  CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`contest_id`) REFERENCES `contest` (`id`),
  CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`problem_id`) REFERENCES `problem` (`id`),
  CONSTRAINT `topic_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (1,'haha','haha','2016-01-14 00:00:00',1,1,1,0),(2,'123','123','2016-01-23 10:13:49',1,1,NULL,0),(3,'阿道夫','asdf','2016-01-23 10:15:34',1,1,1,0),(4,'gggg','gggg','2016-01-23 05:04:48',1,NULL,1,0),(5,'gggg','ggggxxx','2016-01-23 05:05:55',1,NULL,1,0);
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `default_language` int(11) DEFAULT NULL,
  `school` varchar(128) DEFAULT NULL,
  `college` varchar(128) DEFAULT NULL,
  `major` varchar(128) DEFAULT NULL,
  `grade` varchar(128) DEFAULT NULL,
  `uclass` varchar(128) DEFAULT NULL,
  `qq` varchar(128) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `nickname` varchar(128) DEFAULT NULL,
  `about_me` text,
  `blog` varchar(128) DEFAULT NULL,
  `total_submit` int(11) DEFAULT NULL,
  `total_accept` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_users_email` (`email`),
  UNIQUE KEY `ix_users_username` (`username`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','the.mid.sea@gmail.com','pbkdf2:sha1:1000$TWJW8zJb$6ec0201f4741c2544635f1e23c7241a84b3ba75c',0,0,'zjut','ZJUT','计算机','234','234','123123','15757115243','杭州 小和山','YMC','天气好～','messyidea.com',4,0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-23 13:19:40
