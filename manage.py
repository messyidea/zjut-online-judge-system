#! /usr/bin/env python
import os
#COV = None

from app import create_app, db
from app.models import User, Role, Problem, Submission
from flask.ext.script import Manager, Shell
from flask.ext.migrate import Migrate, MigrateCommand
import datetime

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)

@app.template_filter('datetimeformat')
def datetimeformat(value):
    return str(datetime.timedelta(seconds=value))

@app.template_test('not_null')
def not_null(value):
    if value:
        return True
    else:
        return False
    # return value != None

def make_shell_context():
    return dict(app=app, db=db, User=User, Role=Role, Problem=Problem, Submission=Submission)
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)


if __name__ == '__main__':
    # app.jinja_env.filters['datetimeformat'] = format_datatime
    manager.run()